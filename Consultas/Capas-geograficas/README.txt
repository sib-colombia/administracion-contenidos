FUENTES: 

DEPARTAMENTOS Y MUNICIPIOS
Departamento Administrativo Nacional de Estadística DANE (2018), Marco Geoestadístico Nacional, Escala: No definida. Datum: MAGNA-SIRGAS), Recuperado de: https://geoportal.dane.gov.co/servicios/descarga-y-metadatos/descarga-mgn-marco-geoestadistico-nacional/

PARQUES
Parques Nacionales Naturales de Colombia (2020), Límite de los Parques Nacionales Naturales de Colombia, Multiescala (1:1000 y 1:100.000). Datum: MAGNA-SIRGAS, Recuperado de: http://mapas.parquesnacionales.gov.co/services/pnn/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=pnn:runap2&maxFeatures=10000&outputFormat=SHAPE-ZIP. Fecha. 2020-07-08.

CORPORACIONES
Instituto Geográfico Agustín Codazzi (2013), Mapa de Corporaciones Autónomas Regionales, de Desarrollo Sostenible y Autoridades Ambientales Urbanas, Escala (1:2.500.000). Datum: MAGNA-SIRGAS.

RESGUARDOS
Agencia Nacional de Tierras (2018), Resguardos Indígenas, Escala: No definida, Recuperado de: https://www.datos.gov.co/Agricultura-y-Desarrollo-Rural/Resguardos-Ind-genas/2wvk-ve5b


