
# Instrucciones de uso del script de consultas


## Instalación

Se requiere de algunos programas y librerías específicos para la ejecución del script. Sigas estos pasos para su instalación.

1. Instale [ANACONDA](https://www.anaconda.com/products/individual) o [Miniconda](https://docs.conda.io/en/latest/miniconda.html) en su equipo. Dirijase a la página de descarga y siga las instrucciones de instalación del programa. 
   
2. Instale las siguientes librerías haciendo uso de Anaconda prompt(Windows) o la terminal de comandos (Mac):

    - conda install pandas
    - conda install geopandas
    - conda install shapely

- Verifique que queden correctamente instaladas con el comando ```conda list``` . Verá una lista de librerías instaladas.

Abra spyder e intente importar geopandas "import geopandas as gpd". Si no se genera ningún mensaje de error, significa que la instalación fue exitosa. 


Tener en cuenta que la instalación de Geopandas  suele generar conflictos con otras librerías como Fiona, además de requerir de la instalación de otras dependencias. Si con el paso anterior presenta inconvenientes con la instalación o si presenta errores al intentar importar geopandas como: 

```No module named 'geopandas', puede que haya incompatibilidad con el paquete de fiona o falta instalar algunas dependencias importantes. Este problema suele presentarse mas en Windows que en otros sistemas. Para ello siga estos pasos.``` 

Siga estas instrucciones:

**En windows**:

    - Diríjase a la página: https://www.lfd.uci.edu/~gohlke/pythonlibs/
    - Descarge los binarios (.whl) de las siguientes librerias.  Asegurese de descargar la versión compatible con el SO (64 o 32 bits) y la versión de python (3.7,3.9, etc.)
                - GDAL
                - pyproj
                - shapely
                - fiona
                - geopandas
    - Abra la consola de Anaconda (Anaconda prompt)
    - Usando los comandos de terminal cd .. muevase a la ubicación de los archivos descargados. Ejemplo. (base) C:\Users\ricardo.ortiz> cd Downloads
    - Una vez esté ahí ejecute los siguientes comandos (uno por uno) y en este mismo orden para iniciar la instalación:
            - pip install GDAL-3.4.1-cp39-cp39-win_amd64.whl
            - pip install pyproj-3.3.0-cp39-cp39-win_amd64
            - pip install Shapely-1.8.1.post1-cp39-cp39-win_amd64
            - pip install Fiona-1.8.21-cp39-cp39-win_amd64
            - pip install geopandas-0.10.2-py2.py3-none-any

El paso a paso lo puede encontrar en este [video](https://www.youtube.com/watch?v=LNPETGKAe0c). Pero tenga en cuenta usar la versión de python de su equipo al momento de instalar los archivos descargados. 

## Ejecución del script

1.  Importe el [script de consutlas](https://gitlab.com/sib-colombia/administracion-contenidos/-/blob/master/Consultas/Colombia_Consultas.py) a Spyder.

    Para verificar la instalación de los paquetes, importelos desde la interfaz de Spyder. Para ello abra Spyder y ejecute los siguentes comandos que encontrará en el script.

    * import pandas as pd
    * import geopandas as gpd
    * from shapely.geometry import Point


2. En el script verá comentadas casi todas las líneas, es importante que lea cada línea o sección de líneas antes de ejecutarlas. Están le dan instrucciones sobre la ejecución o sobre el tipo de consulta que resuelven. 
3. Ejecute los comandos siguiendo las instrucciones del script según el tipo de consulta que desea resolver.

