# -*- coding: utf-8 -*-
"""
Created: 2018-10-31
Last update: 2020-05-20

@author: camila.plata

"""

import pandas as pd
import os
import numpy as np



print(os.getcwd())
#Establezca la carpeta de las cifras como directorio de trabajo
os.chdir("C:\\Cifras_Colombia\\Cifras_Corporaciones_202108")

#Ingrese el área asociada a las cifras y el periodo de los datos, esta información se incluirá al nombre de los archivos
nombre='Parques2020'

#Carga del archivo de datos con los datos base previamente limpiados según criterios geográficos y taxonómicos
#registros = pd.read_csv('Boyaca_data_2019T4.csv', encoding = "utf8") #leer archivo csv
registros = pd.read_table('DatosCorporaciones2020.txt', encoding = "utf8") #Leer archivo txt


#---------------------------------------------Ajuster preeliminares de los datos -----------------------------------------------

#staProv_divipola = pd.read_table('DIVIPOLA_2020311_unique.txt', encoding = "utf8")
#list(registros.columns) 
#
##reajustar campo stateProvince
#registros['verbatimStateProvince']=registros['stateProvince']
#registros['stateProvince']=registros.stateProvince.str.replace('"','')
#registros=pd.merge(registros,staProv_divipola, on='stateProvince',how='left') 
#registros['stateProvince']=registros['stateProvinceDivipola'].mask(pd.isnull, registros['Departamento-ubicacionCoordenada'])
##

#Ajuste de las categorias threathStatus para asegurar integridad de las cifras MADS e UICN en los conteos
registros.threatStatus_UICN=registros.threatStatus_UICN + '_UICN'
registros.threatStatus_MADS=registros.threatStatus_MADS + '_MADS'


#SubsetTaxonómico si se necesitan cifras totales por reinos
#registros= registros[(registros['kingdom'] =='Animalia')]


#----------------------------------------------------Cifras totales -------------------------------------------------------------
#Crear variables para los conteos por especie
v01 = registros[registros['species'].notna()]#creamos una tabla que excluya las filas cuyo valor de especie sea nulo
v02 = v01.drop_duplicates('species').sort_values(by=['species'])#valores únicos de especie  

tot_cifras=pd.DataFrame(index=[0])
tot_cifras['registros']=registros['gbifID'].count()
tot_cifras['especies']=v02['gbifID'].count()
tot_cifras.to_csv(nombre+'cifrasTotales.txt',sep='\t')
tot_cifras.to_excel(nombre +'cifrasTotales.xlsx', sheet_name='tot_cifras')

#---------------------------------------------Cifras generales geográficas ----------------------------------------------------

# Definir el alcance de las cifras geográficas

g='categoria' # seleccionar entre stateProvince para calcular cifras Nacionales
#g='county' # seleccionar entre county para cifras departamentales

#Cifras registros por entidad geográfica
geo_rb_tot= registros.groupby(g)['gbifID'].count()
geo_rb_tot= geo_rb_tot.to_frame(name = 'registros').reset_index()


#Cifras especies por entidad geográfica
geo_sp_tot= registros.groupby([g,'species'])['species'].count()
geo_sp_tot= geo_sp_tot.to_frame(name = 'registros').reset_index()
geo_sp_tot= geo_sp_tot.groupby([g])['species'].count()
geo_sp_tot =geo_sp_tot.to_frame(name = 'species').reset_index()

#---------------------------------------------Cifras generales organizaciones publicadoras-----------------------------------

# Cifras registros por organización publicadora
ent_rb= registros.groupby('organization')['gbifID'].count()
ent_rb=ent_rb.to_frame(name = 'registros').reset_index()

# Cifras especies por organización publicadora
ent_sp= registros.groupby(['organization','species'])['species'].count()
ent_sp= ent_sp.to_frame(name = 'registros').reset_index()
ent_sp= ent_sp.groupby(['organization'])['species'].count() 
ent_sp= ent_sp.to_frame(name = 'Especies').reset_index()

# Creación dataframe citras totales organizaciones publicadoras
ent_tot=pd.merge(ent_rb,ent_sp, on=['organization'],how='left')
ent_tot.to_csv(nombre+'cifrasEntidades.txt',sep='\t')
ent_tot.to_excel(nombre+'cifrasEntidades.xlsx', sheet_name='cifrasEntidades')

#-------------------------------------------------Loop calculo cifras -----------------------------------------------------

# Crear listas con las categorías sobre als cuales se corre el loop
var =['threatStatus_UICN','isInvasive_mads','appendixCITES','threatStatus_MADS','exotic','isInvasive_griis','endemic','migratory']
taxon = ['kingdom','phylum','class','order','family','genus','species'] # define una lista con todos los taxones

## Creación de dataframes vacíos para guardar las cifras generadas en el loop
#registros
rb_tax=pd.DataFrame()
rb_the=pd.DataFrame()
rb_tax_the=pd.DataFrame()
rb_tax_cat=pd.DataFrame()

# especies
sp_tax=pd.DataFrame()
sp_the=pd.DataFrame()
sp_tax_the=pd.DataFrame()
sp_tax_cat=pd.DataFrame()

# geográficas
geo_the_rb=pd.DataFrame() 
geo_the_sp=pd.DataFrame()
geo_cat_rb=pd.DataFrame()
geo_cat_sp=pd.DataFrame()


#Inicio de los ciclos para calculo de cifras
i=0
j=0   

 
for i in var:
        
        ## La primera aprte del loop recorre las temáticas
        
        # Cifras registros
        rb = registros.groupby(i)['gbifID'].count()
        rb_num = rb.to_frame(name = 'registros' ).reset_index()
        rb_num['thematic']=i
        rb_num=rb_num.rename(columns={i:'category'})
        rb_the=pd.concat([rb_the,rb_num])
        
        #Cifras especies
        sp = v02.groupby(i)['gbifID'].count()
        sp_num = sp.to_frame(name = 'especies' ).reset_index()
        sp_num['thematic']=i
        sp_num=sp_num.rename(columns={i:'category'})
        sp_the=pd.concat([sp_the,sp_num])

        #Cifras  por entidad geográfica
        geo_rb= registros.groupby([g,i])[i].count()
        geo_rb= geo_rb.to_frame(name = 'registros').reset_index()
        geo_rb['thematic']=i
        geo_rb=geo_rb.rename(columns={i:'category'})
        geo_cat_rb=pd.concat([geo_cat_rb,geo_rb])
      
        #Conteo de especies por categorías de cada temática
        geo_sp= registros.groupby([g,i,'species']).size().reset_index()
        geo_sp=geo_sp.groupby([g,i]).size()
        geo_sp= geo_sp.to_frame(name = 'especies').reset_index()
        geo_sp['thematic']=i
        geo_sp= geo_sp.rename(columns={i:'category'})
        geo_cat_sp=pd.concat([geo_cat_sp, geo_sp])
        
        #Conteo de total registros biológicos para todas las categorías de cada temática
        geo_rb = registros.groupby([g])[i].count()
        geo_rb= geo_rb.to_frame(name = 'registros').reset_index()
        geo_rb['thematic']=i
        geo_rb=geo_rb.rename(columns={i:'category'})
        geo_the_rb=pd.concat([geo_the_rb,geo_rb])
              
        #Conteo de total especies únicas para todas las categorías
        geo_sp= registros.groupby([g,i,'species']).size().reset_index()
        geo_sp= geo_sp.groupby([g])[i].count()
        geo_sp= geo_sp.to_frame(name = 'species').reset_index()
        geo_sp['thematic']=i
        geo_sp=geo_sp.rename(columns={i:'category'})
        geo_the_sp=pd.concat([geo_the_sp,geo_sp])
        
        for j in taxon:
            
            ## La segunda parte del loop recorre las taxonomía

            #registros
            rb = registros.groupby(j)['gbifID'].count()
            rb_num = rb.to_frame(name = 'registros' ).reset_index()
            rb_num['taxonRank']=j
            rb_num=rb_num.rename(columns={j:'grupoTax'})
            rb_tax=pd.concat([rb_tax,rb_num])

            rb = registros.groupby(j)[i].count()
            rb_num = rb.to_frame(name = 'registros' ).reset_index()
            rb_num['thematic']=i
            rb_num['taxonRank']=j
            rb_num=rb_num.rename(columns={j:'grupoTax'})
            rb_num=rb_num.rename(columns={i:'category'})
            rb_tax_the=pd.concat([rb_tax_the,rb_num])

            rb = registros.groupby([j,i])[i].count()
            rb_num = rb.to_frame(name = 'registros' ).reset_index()
            rb_num['thematic']=i
            rb_num['taxonRank']=j
            rb_num=rb_num.rename(columns={j:'grupoTax'})
            rb_num=rb_num.rename(columns={i:'category'})
            rb_tax_cat=pd.concat([rb_tax_cat,rb_num])
            
            #especies
            sp = v02.groupby(j)['gbifID'].count()
            sp_num = sp.to_frame(name = 'especies' ).reset_index()
            sp_num['taxonRank']=j
            sp_num=sp_num.rename(columns={j:'grupoTax'})
            sp_tax=pd.concat([sp_tax,sp_num])
            
            sp = v02.groupby(j)[i].count()
            sp_num = sp.to_frame(name = 'especies' ).reset_index()
            sp_num['thematic']=i
            sp_num['taxonRank']=j
            sp_num=sp_num.rename(columns={j:'grupoTax'})
            sp_num=sp_num.rename(columns={i:'category'})
            sp_tax_the=pd.concat([sp_tax_the,sp_num])
                     
            sp = v02.groupby([j,i])[i].count()
            sp_num = sp.to_frame(name = 'especies' ).reset_index()
            sp_num['thematic']=i
            sp_num['taxonRank']=j
            sp_num=sp_num.rename(columns={j:'grupoTax'})
            sp_num=sp_num.rename(columns={i:'category'})
            sp_tax_cat=pd.concat([sp_tax_cat,sp_num])

#Fin de los ciclos para realizar conteos

# Guardar archivos de resultado sobre los cuales se puede recalcualr cifrar según los grupos biológicos
rb_tax.to_csv('rb_tax.txt',sep='\t')
sp_tax.to_csv('sp_tax.txt',sep='\t')
rb_tax_the.to_csv('rb_tax_the.txt',sep='\t')
sp_tax_the.to_csv('sp_tax_the.txt',sep='\t')
rb_tax_cat.to_csv('rb_tax_cat.txt',sep='\t')
sp_tax_cat.to_csv('sp_tax_cat.txt',sep='\t')

#------------------------------Organización en un único dataframe de las cifras temáticas totales--------------------------------

#Merging total rb and sp per thematic x category, ver posibilidad de reorganizar y sacar total por temática
the_cifras=pd.merge(rb_the,sp_the, on=['thematic','category'],how='left') ##### file to use total count per category????
the_cifras.to_csv(nombre+'cifrasTematicasTotales.txt',sep='\t')
the_cifras.to_excel(nombre+'cifrasTematicasTotales.xlsx', sheet_name='the_cifras')
# falta reorganizar columnas

#------------------------------Organización en un único dataframe de las cifras geográficas--------------------------------------

geo_the_rb=geo_the_rb.pivot(g,'thematic').fillna(0)
geo_the_sp=geo_the_sp.pivot(g,'thematic').fillna(0)

geo_cat_rb=geo_cat_rb.drop(['thematic'],axis=1)
geo_cat_rb=geo_cat_rb.pivot(g,'category').fillna(0)

geo_cat_sp=geo_cat_sp.drop(['thematic'],axis=1)
geo_cat_sp=geo_cat_sp.pivot(g,'category').fillna(0)

geo_tot=pd.merge(geo_rb_tot,geo_sp_tot,on=g,how='left').merge(geo_the_rb,on=g,how='left').merge(geo_the_sp,on=g,how='left')
geo_tot=pd.merge(geo_tot,geo_cat_rb,on=g,how='left').merge(geo_cat_sp,on=g,how='left') #final file to use

#----------------------Transformación cifras por categoría taxonómica a cifras por grupos biológicos------------------------------

## Carga de archivos que relacionan la taxonomía con los grupos biológicos, selecione entre las opciones disponibles
            
#Grupos biológicos Cifras Nacionales
gbio1 = pd.read_table('gruposBiologicosCifrasSiB_G1.txt', encoding = "utf8")
gbio2 = pd.read_table('gruposBiologicosCifrasSiB_G2.txt', encoding = "utf8")

#Grupos biológicos Cifras Ventanas Regionales Aréas Continentales
#gbio1 = pd.read_table('gruposBiologicosCifrasSiB_VRC_1.txt', encoding = "utf8")
#gbio2 = pd.read_table('gruposBiologicosCifrasSiB_VRC_2.txt', encoding = "utf8")

#Resumen cifras por grupo biológico
rb_gb1=pd.merge(rb_tax,gbio1, on=['grupoTax','taxonRank'],how='left') #genera duplicados
rb_gb1=rb_gb1.drop_duplicates()
rb_gb1=rb_gb1[['registros','grupoBio']]
rb_cifras1=rb_gb1.groupby('grupoBio').sum().reset_index()

rb_gb2=pd.merge(rb_tax,gbio2, on=['grupoTax','taxonRank'],how='left') #genera duplicados
rb_gb2=rb_gb2.drop_duplicates()
rb_gb2=rb_gb2[['registros','grupoBio']]
rb_cifras2=rb_gb2.groupby('grupoBio').sum().reset_index()

sp_gb1=pd.merge(sp_tax,gbio1, on=['grupoTax','taxonRank'],how='left')
sp_gb1=sp_gb1.drop_duplicates()
sp_gb1=sp_gb1[['especies','grupoBio']]
sp_cifras1=sp_gb1.groupby('grupoBio').sum().reset_index()

sp_gb2=pd.merge(sp_tax,gbio2, on=['grupoTax','taxonRank'],how='left')
sp_gb2=sp_gb2.drop_duplicates()
sp_gb2=sp_gb2[['especies','grupoBio']]
sp_cifras2=sp_gb2.groupby('grupoBio').sum().reset_index()

tax_cifras1=pd.merge(rb_cifras1,sp_cifras1, on='grupoBio',how='left') #file to use
tax_cifras2=pd.merge(rb_cifras2,sp_cifras2, on='grupoBio',how='left') #file to use


#Resumen cifras por grupo biológico y temática
rb_tax_the_gb1=pd.merge(rb_tax_the,gbio1, on=['grupoTax','taxonRank'],how='left')
rb_tax_the_gb1=rb_tax_the_gb1.drop_duplicates()
rb_tax_the_gb1=rb_tax_the_gb1[['registros','grupoBio','thematic']]
rb_tax_the_cifras1=rb_tax_the_gb1.groupby(['grupoBio','thematic']).sum().reset_index()

rb_tax_the_gb2=pd.merge(rb_tax_the,gbio2, on=['grupoTax','taxonRank'],how='left')
rb_tax_the_gb2=rb_tax_the_gb2.drop_duplicates()
rb_tax_the_gb2=rb_tax_the_gb2[['registros','grupoBio','thematic']]
rb_tax_the_cifras2=rb_tax_the_gb2.groupby(['grupoBio','thematic']).sum().reset_index()

sp_tax_the_gb1=pd.merge(sp_tax_the,gbio1, on=['grupoTax','taxonRank'],how='left')
sp_tax_the_gb1=sp_tax_the_gb1.drop_duplicates()
sp_tax_the_gb1=sp_tax_the_gb1[['especies','grupoBio','thematic']]
sp_tax_the_cifras1=sp_tax_the_gb1.groupby(['grupoBio','thematic']).sum().reset_index()

sp_tax_the_gb2=pd.merge(sp_tax_the,gbio2, on=['grupoTax','taxonRank'],how='left')
sp_tax_the_gb2=sp_tax_the_gb2.drop_duplicates()
sp_tax_the_gb2=sp_tax_the_gb2[['especies','grupoBio','thematic']]
sp_tax_the_cifras2=sp_tax_the_gb2.groupby(['grupoBio','thematic']).sum().reset_index()

tax_the_cifras1=pd.merge(rb_tax_the_cifras1,sp_tax_the_cifras1, on=['grupoBio','thematic'],how='left')
tax_the_cifras1=tax_the_cifras1.pivot('grupoBio','thematic').fillna(0) #file to use

tax_the_cifras2=pd.merge(rb_tax_the_cifras2,sp_tax_the_cifras2, on=['grupoBio','thematic'],how='left')
tax_the_cifras2=tax_the_cifras2.pivot('grupoBio','thematic').fillna(0) #file to use


#Resumen cifras de especies y registros  por grupo biológico y por categoría dentro de cada temática
rb_tax_cat_gb1=pd.merge(rb_tax_cat,gbio1, on=['grupoTax','taxonRank'],how='left')
rb_tax_cat_gb1=rb_tax_cat_gb1.drop_duplicates()
rb_tax_cat_gb1=rb_tax_cat_gb1[['registros','grupoBio','thematic','category']]
rb_tax_cat_cifras1=rb_tax_cat_gb1.groupby(['grupoBio','thematic','category']).sum().reset_index()

rb_tax_cat_gb2=pd.merge(rb_tax_cat,gbio2, on=['grupoTax','taxonRank'],how='left')
rb_tax_cat_gb2=rb_tax_cat_gb2.drop_duplicates()
rb_tax_cat_gb2=rb_tax_cat_gb2[['registros','grupoBio','thematic','category']]
rb_tax_cat_cifras2=rb_tax_cat_gb2.groupby(['grupoBio','thematic','category']).sum().reset_index()

sp_tax_cat_gb1=pd.merge(sp_tax_cat,gbio1, on=['grupoTax','taxonRank'],how='left')
sp_tax_cat_gb1=sp_tax_cat_gb1.drop_duplicates()
sp_tax_cat_gb1=sp_tax_cat_gb1[['especies','grupoBio','thematic','category']]
sp_tax_cat_cifras1=sp_tax_cat_gb1.groupby(['grupoBio','thematic','category']).sum().reset_index()

sp_tax_cat_gb2=pd.merge(sp_tax_cat,gbio2, on=['grupoTax','taxonRank'],how='left')
sp_tax_cat_gb2=sp_tax_cat_gb2.drop_duplicates()
sp_tax_cat_gb2=sp_tax_cat_gb2[['especies','grupoBio','thematic','category']]
sp_tax_cat_cifras2=sp_tax_cat_gb2.groupby(['grupoBio','thematic','category']).sum().reset_index()

tax_cat_cifras1=pd.merge(rb_tax_cat_cifras1,sp_tax_cat_cifras1, on=['grupoBio','thematic','category'],how='left')
tax_cat_cifras1=tax_cat_cifras1.drop(['thematic'],axis=1)
tax_cat_cifras1=tax_cat_cifras1.pivot('grupoBio','category').fillna(0) #file to use #acá es donde salen desorganizadas las categorias

tax_cat_cifras2=pd.merge(rb_tax_cat_cifras2,sp_tax_cat_cifras2, on=['grupoBio','thematic','category'],how='left')
tax_cat_cifras2=tax_cat_cifras2.drop(['thematic'],axis=1)
tax_cat_cifras2=tax_cat_cifras2.pivot('grupoBio','category').fillna(0) #file to use #acá es donde salen desorganizadas las categorias

gBio_tot1=pd.merge(tax_cifras1,tax_the_cifras1,on='grupoBio',how='left').merge(tax_cat_cifras1,on='grupoBio',how='left') ###FinalBiologicalGroupFile
gBio_tot2=pd.merge(tax_cifras2,tax_the_cifras2,on='grupoBio',how='left').merge(tax_cat_cifras2,on='grupoBio',how='left') ###FinalBiologicalGroupFile

gBio_tot=pd.concat([gBio_tot1,gBio_tot2])


#------------------------Renombrar y reorganizar las columnas del dataframe de cifras por grupo biológico-------------------

#Según los grupos biológicos y el área geográfica asociadas a las cifras,las categorías temáticas con información cambian
# y  por lo tanto la cantidad de columnas.
# Liste primero las columans y verifique que concida con la lista para renombrar y reorganizar
# Si no coinciden copie la lista en comentarios y use una versión modificada incluyendo solo aquellas columnas validas para sus cifras
list(gBio_tot.columns) 


#Lista base para renombrar copiela y modifíquela según el alcance de las cifras que esté generando
#Renombrar columnas
#gBio_tot.columns=[
# 'especies',
# 'grupoBio',             
# 'registros',
# 'registrosCites',
# 'registrosEndemicas',
# 'registrosExoticas',
# 'registrosisInvasive_griis',
# 'registrosisInvasive_mads',
# 'registrosMigratorio',
# 'registrosAmenaza',
# 'registrosthreatStatus_UICN',
# 'especiesCites',
# 'especiesEndemicas',
# 'especiesExoticas',
# 'especiesisInvasive_griis',
# 'especiesisInvasive_mads',
# 'especiesMigratorio',
# 'especiesAmenaza',
# 'especiesthreatStatus_UICN',
# 'registrosCR',
# 'registrosCR_UICN',
# 'registrosDD_UICN',
# 'registrosEN',
# 'registrosEN_UICN',
# 'registrosEW_UICN',
# 'registrosEX_UICN',
# 'registrosEndemicas2',
# 'registrosErrático',
# 'registrosExoticas2',
# 'registrosCitesI',
# 'registrosCitesI/II',
# 'registrosCitesII',
# 'registrosCitesIII',
# 'registrosInvasora',
# 'registrosInvasora GRIIS',
# 'registrosLC_UICN',
# 'registrosLR/cd_UICN',
# 'registrosMigratorias',
# 'registrosNT_UICN',
# 'registrosResidente',
# 'registrosVU',
# 'registrosVU_UICN',
#'especiesCR',
#'especiesCR_UICN',
#'especiesDD_UICN',
#'especiesEN',
#'especiesEN_UICN',
#'especiesEW_UICN',
#'especiesEX_UICN',
# 'especiesEndemicas2',
# 'especiesErrático',
# 'especiesExoticas2',
#'especiesCitesI',
#'especiesCitesI/II',
#'especiesCitesII',
#'especiesCitesIII',
# 'especiesInvasora',
# 'especiesInvasora GRIIS',
#'especiesLC_UICN',
#'especiesLR/cd_UICN',
#'especiesMigratorias',
#'especiesNT_UICN',
#'especiesResidente',
#'especiesVU',
#'especiesVU_UICN']

gBio_tot.columns=[
 'especies',
 'grupoBio',             
 'registros',
 'registrosCites',
 'registrosEndemicas',
 'registrosExoticas',
 'registrosisInvasive_griis',
 'registrosisInvasive_mads',
 'registrosMigratorio',
 'registrosAmenaza',
 'registrosthreatStatus_UICN',
 'especiesCites',
 'especiesEndemicas',
 'especiesExoticas',
 'especiesisInvasive_griis',
 'especiesisInvasive_mads',
 'especiesMigratorio',
 'especiesAmenaza',
 'especiesthreatStatus_UICN',
 'registrosCR',
 'registrosCR_UICN',
 'registrosDD_UICN',
 'registrosEN',
 'registrosEN_UICN',
 'registrosEW_UICN',
 'registrosEX_UICN',
 'registrosEndemicas2',
 #'registrosErrático',
 'registrosExoticas2',
 'registrosCitesI',
 'registrosCitesI/II',
 'registrosCitesII',
 'registrosCitesIII',
 'registrosInvasora GRIIS',
 'registrosInvasora',
 'registrosLC_UICN',
 'registrosLR/cd_UICN',
 'registrosLR/lc_UICN',
 'registrosLR/nt_UICN',
 'registrosMigratorias',
 'registrosNT_UICN',
 #'registrosResidente',
 'registrosVU',
 'registrosVU_UICN',
'especiesCR',
'especiesCR_UICN',
'especiesDD_UICN',
'especiesEN',
'especiesEN_UICN',
'especiesEW_UICN',
'especiesEX_UICN',
'especiesEndemicas2',
#'especiesErrático',
'especiesExoticas2',
'especiesCitesI',
'especiesCitesI/II',
'especiesCitesII',
'especiesCitesIII',
 'especiesInvasora GRIIS',
  'especiesInvasora',
'especiesLC_UICN',
'especiesLR/cd_UICN',
 'especiesLR/lc_UICN',
 'especiesLR/nt_UICN',
'especiesMigratorias',
'especiesNT_UICN',
#'especiesResidente',
'especiesVU',
'especiesVU_UICN']


#Eliminar columnas innecesarias
gBio_tot=gBio_tot.drop(['registrosMigratorio','especiesMigratorio',
                         'registrosEndemicas2', 'registrosExoticas2','especiesEndemicas2', 'especiesExoticas2',
                          'registrosisInvasive_griis','registrosisInvasive_mads', 'especiesisInvasive_griis','especiesisInvasive_mads'], axis=1)

##Reorganizar columnas
#Para cifras a nivel de país y córdoba donde el apendice cites I/II aplica
#gBio_tot=gBio_tot[['grupoBio',
# 'especies',
# 'registros',
# 'especiesAmenaza',
# 'especiesEN',
# 'especiesCR',
# 'especiesVU',
# 'registrosAmenaza',
# 'registrosCR',
# 'registrosEN',
# 'registrosVU',
# 'especiesCites',
# 'especiesCitesI',
# 'especiesCitesI/II',
# 'especiesCitesII',
# 'especiesCitesIII',
# 'registrosCites',
# 'registrosCitesI',
# 'registrosCitesI/II',
# 'registrosCitesII',
# 'registrosCitesIII',
# 'especiesEndemicas',
# 'especiesExoticas',
# 'especiesMigratorias',
# 'registrosEndemicas',
# 'registrosExoticas',
# 'registrosMigratorias',
# 'especiesInvasora',
# 'registrosInvasora',
# 'especiesInvasora GRIIS',
# 'registrosInvasora GRIIS',
# 'especiesthreatStatus_UICN',
# 'especiesEX_UICN',
# 'especiesEW_UICN',
# 'especiesCR_UICN',
# 'especiesEN_UICN',
# 'especiesVU_UICN',
# 'especiesNT_UICN',
# 'especiesLC_UICN',
# 'especiesLR/cd_UICN',
# 'especiesDD_UICN',
# 'registrosthreatStatus_UICN',
# 'registrosEX_UICN',
# 'registrosEW_UICN',
# 'registrosCR_UICN',
# 'registrosEN_UICN',
# 'registrosVU_UICN',
# 'registrosNT_UICN',
# 'registrosLC_UICN',
# 'registrosLR/cd_UICN',
# 'registrosDD_UICN']]

gBio_tot=gBio_tot[['grupoBio',
 'registros',
 'especies',
 'especiesAmenaza',
 'especiesEN',
 'especiesCR',
 'especiesVU',
 'registrosAmenaza',
 'registrosCR',
 'registrosEN',
 'registrosVU',
 'especiesCites',
 'especiesCitesI',
 'especiesCitesII',
 'especiesCitesIII',
 'registrosCites',
 'registrosCitesI',
 'registrosCitesII',
 'registrosCitesIII',
 'especiesEndemicas',
 'especiesExoticas',
 'especiesMigratorias',
 'registrosEndemicas',
 'registrosExoticas',
 'registrosMigratorias',
 'especiesInvasora',
 'registrosInvasora',
 'especiesInvasora GRIIS',
 'registrosInvasora GRIIS',
 'especiesthreatStatus_UICN',
 'especiesEX_UICN',
 'especiesEW_UICN',
 'especiesCR_UICN',
 'especiesEN_UICN',
 'especiesVU_UICN',
 'especiesNT_UICN',
 'especiesLC_UICN',
 'especiesDD_UICN',
 'registrosthreatStatus_UICN',
 'registrosEX_UICN',
 'registrosEW_UICN',
 'registrosCR_UICN',
 'registrosEN_UICN',
 'registrosVU_UICN',
 'registrosNT_UICN',
 'registrosLC_UICN',
 'registrosDD_UICN']]

#Evirar .0 al final de las cifras
gBio_tot=gBio_tot.astype(str)
gBio_tot=gBio_tot.replace(to_replace='\.0+$',value="",regex=True)

#Cifras finales por grupo biológico y temática
gBio_tot.to_csv(nombre+'cifrasGruposBiologicos.txt',sep='\t')
gBio_tot.to_excel(nombre+'cifrasGruposBiologicos.xlsx', sheet_name='cifrasGruposBiologicos')


#------------------------------Renombrar y reorganizar las columnas del dataframe de cifras geográficas--------------------------------------

#Según los grupos biológicos y el área geográfica asociadas a las cifras,las categorías temáticas con información cambian
# y  por lo tanto la cantidad de columnas.
# Liste primero las columans y verifique que concida con la lista para renombrar y reorganizar
# Si no coinciden copie la lista en comentarios y use una versión modificada incluyendo solo aquellas columnas validas para sus cifras

list(geo_tot.columns) 

#geo_tot.columns=[g,
# 'registros',
# 'especies',
# 'registrosCites',
# 'registrosEndemicas',
# 'registrosExoticas',
# 'registrosisInvasive_griis',
# 'registrosisInvasive_mads',
# 'registrosMigratorio',
# 'registrosAmenaza',
# 'registrosthreatStatus_UICN',
# 'especiesCites',
# 'especiesEndemicas',
# 'especiesExoticas',
# 'especiesisInvasive_griis',
# 'especiesisInvasive_mads',
# 'especiesMigratorio',
# 'especiesAmenaza',
# 'especiesthreatStatus_UICN',
# 'registrosCR',
# 'registrosCR_UICN',
# 'registrosDD_UICN',
# 'registrosEN',
# 'registrosEN_UICN',
# 'registrosEW_UICN',
# 'registrosEX_UICN',
# 'registrosEndemicas2',
# 'registrosErrático',
# 'registrosExoticas2',
# 'registrosCitesI',
# 'registrosCitesI/II',
# 'registrosCitesII',
# 'registrosCitesIII',
# 'registrosInvasora',
# 'registrosInvasora GRIIS',
# 'registrosLC_UICN',
# 'registrosLR/cd_UICN',
# 'registrosMigratorias',
# 'registrosNT_UICN',
# 'registrosResidente',
# 'registrosVU',
# 'registrosVU_UICN',
# 'especiesCR',
# 'especiesCR_UICN',
# 'especiesDD_UICN',
# 'especiesEN',
# 'especiesEN_UICN',
# 'especiesEW_UICN',
# 'especiesEX_UICN',
# 'especiesEndemicas2',
# 'especiesErrático',
# 'especiesExoticas2',
# 'especiesCitesI',
# 'especiesCitesI/II',
# 'especiesCitesII',
# 'especiesCitesIII',
# 'especiesInvasora',
# 'especiesInvasora GRIIS',
# 'especiesLC_UICN',
# 'especiesLR/cd_UICN',
# 'especiesMigratorias',
# 'especiesNT_UICN',
# 'especiesResidente',
# 'especiesVU_MADS',
# 'especiesVU']


geo_tot.columns=[g,
 'registros',
 'especies',
 'registrosCites',
 'registrosEndemicas',
 'registrosExoticas',
 'registrosisInvasive_griis',
 'registrosisInvasive_mads',
 'registrosMigratorio',
 'registrosAmenaza',
 'registrosthreatStatus_UICN',
 'especiesCites',
 'especiesEndemicas',
 'especiesExoticas',
 'especiesisInvasive_griis',
 'especiesisInvasive_mads',
 'especiesMigratorio',
 'especiesAmenaza',
 'especiesthreatStatus_UICN',
 'registrosCR',
 'registrosCR_UICN',
 'registrosDD_UICN',
 'registrosEN',
 'registrosEN_UICN',
 'registrosEW_UICN',
 'registrosEX_UICN',
 'registrosEndemicas2',
 #'registrosErrático',
 'registrosExoticas2',
 'registrosCitesI',
 'registrosCitesI/II',
 'registrosCitesII',
 'registrosCitesIII',
  'registrosInvasora GRIIS',
 'registrosInvasora',
 'registrosLC_UICN',
 'registrosLR/cd_UICN',
 'registrosLR/lc_UICN',
 'registrosLR/nt_UICN',
 'registrosMigratorias',
 'registrosNT_UICN',
 #'registrosResidente',
 'registrosVU',
 'registrosVU_UICN',
'especiesCR',
'especiesCR_UICN',
'especiesDD_UICN',
'especiesEN',
'especiesEN_UICN',
'especiesEW_UICN',
'especiesEX_UICN',
'especiesEndemicas2',
#'especiesErrático',
'especiesExoticas2',
'especiesCitesI',
'especiesCitesI/II',
'especiesCitesII',
'especiesCitesIII',
 'especiesInvasora GRIIS',
 'especiesInvasora',
'especiesLC_UICN',
'especiesLR/cd_UICN',
 'especiesLR/lc_UICN',
 'especiesLR/nt_UICN',
'especiesMigratorias',
'especiesNT_UICN',
#'especiesResidente',
'especiesVU',
'especiesVU_UICN']

#Eliminar columnas innecesarias
geo_tot=geo_tot.drop(['registrosMigratorio','especiesMigratorio',
                         'registrosEndemicas2', 'registrosExoticas2','especiesEndemicas2', 'especiesExoticas2',
                          'registrosisInvasive_griis','registrosisInvasive_mads','especiesisInvasive_griis','especiesisInvasive_mads'], axis=1)



##Reorganizar columnas
#Para cifras a nivel de país y córdoba donde el apendice cites I/II aplica
#geo_tot=geo_tot[['g,
# 'especies',
# 'registros',
# 'especiesAmenaza',
# 'especiesEN',
# 'especiesCR',
# 'especiesVU',
# 'registrosAmenaza',
# 'registrosCR',
# 'registrosEN',
# 'registrosVU',
# 'especiesCites',
# 'especiesCitesI',
# 'especiesCitesI/II',
# 'especiesCitesII',
# 'especiesCitesIII',
# 'registrosCites',
# 'registrosCitesI',
# 'registrosCitesI/II',
# 'registrosCitesII',
# 'registrosCitesIII',
# 'especiesEndemicas',
# 'especiesExoticas',
# 'especiesMigratorias',
# 'registrosEndemicas',
# 'registrosExoticas',
# 'registrosMigratorias',
# 'especiesInvasora',
# 'registrosInvasora',
# 'especiesInvasora GRIIS',
# 'registrosInvasora GRIIS',
# 'especiesthreatStatus_UICN',
# 'especiesEX_UICN',
# 'especiesEW_UICN',
# 'especiesCR_UICN',
# 'especiesEN_UICN',
# 'especiesVU_UICN',
# 'especiesNT_UICN',
# 'especiesLC_UICN',
# 'especiesLR/cd_UICN',
# 'especiesDD_UICN',
# 'registrosthreatStatus_UICN',
# 'registrosEX_UICN',
# 'registrosEW_UICN',
# 'registrosCR_UICN',
# 'registrosEN_UICN',
# 'registrosVU_UICN',
# 'registrosNT_UICN',
# 'registrosLC_UICN',
# 'registrosLR/cd_UICN',
# 'registrosDD_UICN']]

geo_tot=geo_tot[[g,
 'especies',
 'registros',
 'especiesAmenaza',
 'especiesEN',
 'especiesCR',
 'especiesVU',
 'registrosAmenaza',
 'registrosCR',
 'registrosEN',
 'registrosVU',
 'especiesCites',
 'especiesCitesI',
 'especiesCitesII',
 'especiesCitesIII',
 'registrosCites',
 'registrosCitesI',
 'registrosCitesII',
 'registrosCitesIII',
 'especiesEndemicas',
 'especiesExoticas',
 'especiesMigratorias',
 'registrosEndemicas',
 'registrosExoticas',
 'registrosMigratorias',
 'especiesInvasora',
 'registrosInvasora',
 'especiesInvasora GRIIS',
 'registrosInvasora GRIIS',
 'especiesthreatStatus_UICN',
 'especiesEX_UICN',
 'especiesEW_UICN',
 'especiesCR_UICN',
 'especiesEN_UICN',
 'especiesVU_UICN',
 'especiesNT_UICN',
 'especiesLC_UICN',
 'especiesDD_UICN',
 'registrosthreatStatus_UICN',
 'registrosEX_UICN',
 'registrosEW_UICN',
 'registrosCR_UICN',
 'registrosEN_UICN',
 'registrosVU_UICN',
 'registrosNT_UICN',
 'registrosLC_UICN',
 'registrosDD_UICN']]


geo_tot=geo_tot.astype(str)
geo_tot=geo_tot.replace(to_replace='\.0+$',value="",regex=True)

#Cifras finales por grupo biológico y temática
geo_tot.to_csv(nombre+'cifrasGeográficas.txt',sep='\t')
geo_tot.to_excel(nombre+'cifrasGeográficas.xlsx', sheet_name='cifrasGeográficas')


