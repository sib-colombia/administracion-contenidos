	category	registros	thematic	especies
0	CR_UICN	15967	threatStatus_UICN	252
1	DD_UICN	10521	threatStatus_UICN	488
2	EN_UICN	36208	threatStatus_UICN	461
3	EW_UICN	1659	threatStatus_UICN	5
4	EX_UICN	55	threatStatus_UICN	3
5	LC_UICN	7341181	threatStatus_UICN	7758
6	LR/cd_UICN	1937	threatStatus_UICN	4
7	LR/lc_UICN	3710	threatStatus_UICN	32
8	LR/nt_UICN	1539	threatStatus_UICN	27
9	NT_UICN	126984	threatStatus_UICN	330
10	VU_UICN	104523	threatStatus_UICN	525
11	Invasora_MADS	4129	isInvasive_mads	17
12	I	18187	appendixCITES	50
13	I/II	50	appendixCITES	1
14	II	1049657	appendixCITES	2539
15	III	44542	appendixCITES	31
16	CR_MADS	20178	threatStatus_MADS	159
17	EN_MADS	51734	threatStatus_MADS	377
18	VU_MADS	108219	threatStatus_MADS	610
19	Exótica	168475	exotic	346
20	Invasora GRIIS	121025	isInvasive_griis	74
21	Endémica	314115	endemic	5937
22	Migratorio	672639	migratory	154
