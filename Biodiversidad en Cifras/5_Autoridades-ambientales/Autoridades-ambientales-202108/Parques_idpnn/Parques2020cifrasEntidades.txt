	organization	registros	Especies
0	A.J. Cook Arthropod Research Collection	743	211.0
1	Academy of Natural Sciences	20996	1665.0
2	Administración de Parques Nacionales, Argentina	32	1.0
3	Aguas de Bogotá S.A. E.S.P.	13280	168.0
4	American Museum of Natural History	38824	1896.0
5	Anadarko Colombia Company	282	14.0
6	Angelo State Natural History Collections (ASNHC)	11	10.0
7	Archbold Biological Station	15	14.0
8	Arctos	70	45.0
9	Arizona State University Biocollections	603	167.0
10	Asociación Bogotana de Ornitología	8675	137.0
11	Asociación Colombiana de Ictiólogos	767	181.0
12	Asociación Colombiana de Piscicultura y Pesca - PISPESCA	4059	25.0
13	Asociación GAICA	3258	161.0
14	Asociación Primatológica Colombiana	1747	31.0
15	Asociación SELVA: Investigación para la Conservación en el Neotrópico	33664	713.0
16	Asociación de Becarios del Casanare	18062	1444.0
17	Asociación para el estudio y conservación de las aves acuáticas en Colombia - Calidris	33241	927.0
18	Auburn University Museum	1629	164.0
19	Auckland War Memorial Museum	35	8.0
20	Australia's Virtual Herbarium	1367	127.0
21	Australian Museum	68	2.0
22	Aïgos SAS	2404	224.0
23	BCCM - Belgian Coordinated Collections of Micro-organisms	36	11.0
24	Bailey-Matthews National Shell Museum	314	182.0
25	Berkeley Natural History Museums	2278	299.0
26	Bernice Pauahi Bishop Museum	43	33.0
27	BioFresh	46	16.0
28	Biodiversity Data Journal	37	11.0
29	Biologiezentrum Linz Oberoesterreich	38	24.0
30	Biotica Consultores Ltda	828	327.0
31	Botanic Garden Meise	2356	1351.0
32	Botanic Garden and Botanical Museum Berlin	9281	2639.0
33	Botanical Garden of Córdoba	2	1.0
34	Botanical Garden, University of Valencia	7	5.0
35	Botanical Institute of Barcelona (IBB-CSIC-ICUB)	405	309.0
36	Botanical Research Institute of Texas	14	13.0
37	Brigham Young University, Arthropod Collection	22	10.0
38	British Antarctic Survey	13	9.0
39	Brown University Herbarium	1	1.0
40	CAR - Corporación Autónoma Regional de Cundinamarca	947	117.0
41	CBGP (UMR INRA, Cirad, IRD, Montpellier SupAgro)	37	5.0
42	CBS Fungal Biodiversity Centre	232	131.0
43	CDMB - Corporación Autónoma Regional Para la Defensa de la Meseta de Bucaramanga	11289	2155.0
44	CRC - Corporación Autónoma Regional del Valle del Cauca	4401	636.0
45	CRQ - Corporación Autónoma Regional del Quindío	148	14.0
46	CSIC-Real Jardín Botánico	38784	5938.0
47	CVS - Corporación Autónoma Regional de los Valles del Sinú y del San Jorge-	3481	338.0
48	Cabildo Verde de Sabana de Torres	525	278.0
49	California Academy of Sciences	9032	2060.0
50	California State University, Long Beach	7	4.0
51	Canadian Biodiversity Information Facility	253	223.0
52	Canadian Museum of Nature	1626	534.0
53	Canadian node of the Ocean Biogeographic Information System (OBIS Canada)	2	1.0
54	Carbones del Cerrejón Limited	9216	328.0
55	Caribbean OBIS Node	1875	257.0
56	Carnegie Museums	14508	905.0
57	Celsia S.A. E.S.P.	35	7.0
58	Centre for Genetic Resources, The Netherlands	47	9.0
59	Centro Internacional de Agricultura Tropical - CIAT	29412	1016.0
60	Centro Nacional de Investigaciones del Café - Cenicafé	3405	89.0
61	Centro de Bioinformática y Biología Computacional de Colombia - BIOS	479	
62	Centro de Ensino São Lucas	8	4.0
63	Centro de Estudios Parasitológicos y de Vectores (CEPAVE)	803	25.0
64	Centro de Referência em Informação Ambiental	1	
65	Cerro Matoso S.A	8914	741.0
66	Charles R. Conner Museum	3	3.0
67	Cheadle Center for Biodiversity and Ecological Restoration	3	1.0
68	Chicago Academy of Sciences	24	9.0
69	Chico State Herbarium	1	1.0
70	Cincinnati Museum Center	2	
71	Cleveland Museum of Natural History	84	45.0
72	Colorado State University, C.P. Gillette Museum of Arthropod Diversity	43	25.0
73	ComisiÃ³n nacional para el conocimiento y uso de la biodiversidad	2079	994.0
74	Comissão Executiva do Plano da Lavoura Cacaueira – Centro de Pesquisas do Cacau	59	54.0
75	Commonwealth Scientific and Industrial Research Organisation	105	
76	Compensation International Progress S.A. -Ciprogress Greenlife-	820	
77	Conare - Corporación Autónoma Regional de las cuencas de los ríos Negro y Nare	12344	1675.0
78	Conservatoire et Jardin botaniques de la Ville de Genève - G	2314	1387.0
79	Coralina - Corporación para el Desarrollo Sostenible del Archipiélago de San Andrés, Providencia y Santa Catalina	65	1.0
80	Corantioquia - Corporación Autónoma Regional del Centro de Antioquia	13777	1891.0
81	Cormacarena - Corporación para el Desarrollo Sostenible del área de Manejo Especial La Macarena	1680	420.0
82	Cornell Lab of Ornithology	6197260	1849.0
83	Cornell University Museum of Vertebrates	1431	537.0
84	Corpoboyacá - Corporación Autónoma Regional de Boyacá	607	39.0
85	Corpocaldas - Corporación Autónoma Regional de Caldas	1662	164.0
86	Corpochivor - Corporación Autónoma Regional de Chivor	114	37.0
87	Corpoguavio - Corporación Autónoma Regional del Guavio	16098	548.0
88	Corporación Centro de Investigación en Palma de Aceite - CENIPALMA	506	1.0
89	Corporación Colombiana de Investigación Agropecuaria - AGROSAVIA	2200	143.0
90	Corporación CorpoGen	150	20.0
91	Corporación Cuenca Verde	308	231.0
92	Corporación Paisajes Rurales	21234	1372.0
93	Corporación San Jorge	51	50.0
94	Corporación Universitaria del Huila	1230	16.0
95	Corporación para la gestión ambiental Biodiversa	1165	87.0
96	Council for Scientific and Industrial Research-Plant Genetic Resources Research Institute (CSIR-PGRRI),Ghana	22	4.0
97	Darder Natural History Museum of Banyoles	6	5.0
98	Delaware Museum of Natural History	1035	389.0
99	Denver Museum of Nature & Science	79	64.0
100	Dep. Biology, Univ. Autónoma de Madrid	2434	25.0
101	Dep. of Plant Biology and Ecology, Univ. Seville	96	10.0
102	Departamento de Farmacología, Farmacognosia & Botánica, Fac. de Farmacia, Univ. Complutense de Madrid	3	3.0
103	Department of Bioscience, Aarhus University	59257	67.0
104	Department of Organisms and Systems Biology. University of Oviedo	2	1.0
105	Department of Plant Biology. Faculty of Biological Sciences. Univ. Murcia	3	3.0
106	Department of Zoology, Cambridge	532	18.0
107	Dept. Of Biology, University of Trieste	2	
108	Depto. de Ciencias Ambientales y Recursos Naturales, Universidad de Alicante	65	46.0
109	Desert Botanical Garden Herbarium	2	1.0
110	Diveboard	97	48.0
111	Duke University Herbarium	371	148.0
112	Embrapa Cenargen	38	10.0
113	Embrapa Semiárido	2	1.0
114	Emporia State University	24	5.0
115	Escola Superior de Agricultura Luiz de Queiroz	7	6.0
116	European Nucleotide Archive (EMBL-EBI)	8052	661.0
117	FIOCRUZ - Oswaldo Cruz Foundation	728	91.0
118	Faculdade de Filosofia, Ciências e Letras de Ribeirão Preto (FFCLRP), Universidade de São Paulo (USP)	625	43.0
119	Facultad de Ciencias Naturales y Museo - U.N.L.P.	313	119.0
120	Fairchild Tropical Botanic Garden	474	128.0
121	Federación Nacional de Cacaoteros	17	
122	Federación Nacional de Cafeteros de Colombia	26804	742.0
123	Field Museum	71498	12039.0
124	Finnish Museum of Natural History	9	4.0
125	FishBase	649	388.0
126	Florida Museum of Natural History	7904	1709.0
127	Fort Hays Sternberg Museum of Natural History	3	3.0
128	Friedrich-Alexander University of Erlangen-Nürnberg	1	
129	Frost Entomological Museum	5	2.0
130	Fundación Alma	282	212.0
131	Fundación Bosques y Humedales	2012	1.0
132	Fundación Botánica y Zoológica de Barranquilla	106	13.0
133	Fundación Centro de Primates	57	1.0
134	Fundación Centro para la Investigación en Sistemas Sostenibles de Producción Agropecuaria - Cipav	1754	44.0
135	Fundación Cerro Bravo	214	127.0
136	Fundación Chimbilako	584	22.0
137	Fundación Colombia Azul	301	11.0
138	Fundación Ecohabitats	739	355.0
139	Fundación Ecológica Fenicia Defensa Natural - FEDENA	1565	341.0
140	Fundación Ecotrópico de Colombia	7277	563.0
141	Fundación Entropika	1302	8.0
142	Fundación Estación Biológica Guayacanal	1203	260.0
143	Fundación Gaia	1846	399.0
144	Fundación Humedales	26805	543.0
145	Fundación Macuáticos Colombia	1	1.0
146	Fundación Malpelo y Otros Ecosistemas Marinos	1320	673.0
147	Fundación Natura Colombia	1165	205.0
148	Fundación Omacha	7480	658.0
149	Fundación Orinoquia Biodiversa (FOB)	2752	208.0
150	Fundación Orinoquía	136	4.0
151	Fundación Panthera Colombia	8186	102.0
152	Fundación Pro-Sierra Nevada de Santa Marta	205	94.0
153	Fundación Reserva Natural La Palmita - Centro de Investigación	5895	551.0
154	Fundación Tortugas del Mar	175	3.0
155	Fundación Trópico	4095	566.0
156	Fundación Trópico Alto	218	94.0
157	Fundación Universidad de Bogotá Jorge Tadeo Lozano	20	3.0
158	Fundación para la Investigación y Conservación Biológica Marina - Ecomares	276	32.0
159	Fundaci贸n Cunaguaro	318	16.0
160	Fundação Zoobotânica do Rio Grande do Sul	17	13.0
161	GBIF-Sweden	5799	2491.0
162	Georg-August-Universität Göttingen, Albrecht-von-Haller-Institut für Pflanzenwissenschaften, Abteilung Systematische Botanik	4558	632.0
163	Ghana Biodiversity Information Facility (GhaBIF)	6	6.0
164	Ghent University	5	4.0
165	Gobernación de San Andrés y Providencia	623	1.0
166	Gothenburg Natural History Museum (GNM)	403	232.0
167	Grupo Energía Bogotá	61111	300.0
168	Harvard University Herbaria	6381	2896.0
169	Hatovial S.A.S	1898	65.0
170	Haus der Natur Salzburg, Museum für Natur und Technik	6	4.0
171	Herbario SANT, Universidade de Santiago de Compostela	5	5.0
172	Herbarium Hamburgense	64	50.0
173	Herbarium LEB Jaime Andrés Rodríguez. Facultad de Ciencias Biológicas y Ambientales. Universidad de León.	2	2.0
174	Herbarium of Université de Montpellier 2, Institut de Botanique	515	376.0
175	Herbarium of the University of Aarhus	424	215.0
176	Humboldt State University	4	4.0
177	INERCO Consultoría Colombia	1090	193.0
178	INRA Antilles-Guyane	1	1.0
179	IRD - Institute of Research for Development	4	3.0
180	Illinois Natural History Survey	1254	114.0
181	Illinois State Museum	5	3.0
182	Insectarium de Montréal	1	1.0
183	Institute of Biodiversity, Animal Health and Comparative Medicine, College of Medical, Veterinary and Life Sciences, University of Glasgow	131	2.0
184	Institute of Marine Sciences (ICM-CSIC)	4	3.0
185	Instituto Agronômico (IAC)	93	38.0
186	Instituto Agronômico de Pernambuco	13	13.0
187	Instituto Amazónico de Investigaciones Científicas - SINCHI	187298	9790.0
188	Instituto Anchietano de Pesquisas/UNISINOS	45	40.0
189	Instituto Colombiano de Medicina Tropical - ICTM	3535	50.0
190	Instituto Federal de Educação, Ciência e Tecnologia do Amazonas	1	1.0
191	Instituto Florestal	1	1.0
192	Instituto Multidisciplinario de Biología Vegetal - IMBIV	214	79.0
193	Instituto Nacional da Mata Atlântica	120	53.0
194	Instituto Nacional de Pesquisas da Amazônia - INPA	2302	1103.0
195	Instituto Nacional de Salud	8633	82.0
196	Instituto Tecnológico Metropolitano	3421	710.0
197	Instituto de Botánica Darwinion - CONICET	156	81.0
198	Instituto de Botânica, São Paulo	895	580.0
199	Instituto de Investigaciones Ambientales del Pacífico John Von Neumann - IIAP	45477	1848.0
200	Instituto de Investigaciones Marinas y Costeras - Invemar	77263	1425.0
201	Instituto de Investigación de Recursos Biológicos Alexander von Humboldt	1027452	18942.0
202	Instituto de Pesquisas Jardim Botanico do Rio de Janeiro	2383	1058.0
203	Instituto para la Investigación y la Preservación del Patrimonio Cultural y Natural del Valle del Cauca - INCIVA	8309	1094.0
204	InvBasa	94	20.0
205	Isagen S.A.S.	19352	581.0
206	James R. Slater Museum of Natural History	31	13.0
207	Jardim Botânico Plantarum	2	2.0
208	Jardin Botanique de Montréal	6	6.0
209	"Jardín Botánico de Bogotá ""José Celestino Mutis"""	37827	3587.0
210	"Jardín Botánico de Cartagena ""Guillermo Piñeres"""	5776	1289.0
211	Jardín Botánico del Quindío	417	197.0
212	Kathryn Kalmbach Herbarium (Denver Botanic Gardens)	2	2.0
213	KwaZulu-Natal Museum	8	3.0
214	Laboratoire de Paléobotanique et Paléoécologie	107	3.0
215	Landcare Research	158	98.0
216	Leibniz Institute DSMZ - German Collection of Microorganisms and Cell Cultures	105	30.0
217	Leibniz Institute of Plant Genetics and Crop Plant Research (IPK)	133	2.0
218	Lomonosov Moscow State University	144	74.0
219	Louisiana State University Herbarium	91	34.0
220	Louisiana State University Museum of Natural Science	1698	587.0
221	Lund Botanical Museum (LD)	362	298.0
222	Lund Museum of Zoology	1152	715.0
223	MGnify	5908	210.0
224	MNHN - Museum national d'Histoire naturelle	13905	5009.0
225	Manchester Museum, The University of Manchester	23	4.0
226	McGill University	5	1.0
227	Michigan State University Herbarium	14	13.0
228	Michigan State University Museum	828	166.0
229	Milwaukee Public Museum	139	54.0
230	Ministerio de Ciencia, Tecnología e Innovación Productiva	15	2.0
231	Mississippi Entomological Museum	21	16.0
232	Missouri Botanical Garden	194207	18208.0
233	Moam Monitoreos Ambientales S.A.S	1781	212.0
234	Monte L. Bean Museum, Brigham Young University	34	7.0
235	Moore Laboratory of Zoology	194	94.0
236	Museo Argentino de Ciencias Naturales	267	156.0
237	Museo Entomológico de Comfenalco - Antioquia	15225	572.0
238	Museo Entomológico de León – MEL	37	9.0
239	Museo Nacional de Costa Rica	1021	530.0
240	Museo de ZoologÃ­a de la Facultad de Ciencias, UNAM	2	1.0
241	Museu Botânico Municipal	1822	1284.0
242	Museu Nacional / UFRJ	1349	477.0
243	Museu Nacional de História Natural e da Ciência	11	
244	Museu Paraense EmÃ­lio Goeldi	717	304.0
245	Museu de Ciències Naturals de Barcelona	308	91.0
246	Museum George Sand et de la Vallee Noire	2	2.0
247	Museum Stavanger	1	
248	Museum and Art Gallery of the Northern Territory	13	
249	Museum and Institute of Zoology, Polish Academy of Sciences	397	124.0
250	Museum für Naturkunde Berlin	59	22.0
251	Museum of Biological Diversity, The Ohio State University	20656	238.0
252	Museum of Comparative Zoology, Harvard University	20187	3750.0
253	Museum of Cultural and Natural History - Central Michigan University	2	1.0
254	Museum of Natural and Cultural History - University of Oregon	3	
255	Museum of Southwestern Biology	654	36.0
256	Museum of Texas Tech University (TTU)	844	78.0
257	Museum of Vertebrate Zoology	5796	855.0
258	Museums Victoria	187	2.0
259	MusÃ©e Zoologique de la Ville de Strasbourg	285	159.0
260	Musée des Confluences	182	50.0
261	Musée national d'histoire naturelle Luxembourg	2	
262	Muséum d'Histoire Naturelle de Bourges	64	46.0
263	Muséum d'Histoire naturelle de Grenoble	2	1.0
264	Muséum d'histoire naturelle de la Ville de Genève - MHNG	2797	175.0
265	N. I. Vavilov Institute of Plant Genetic Resources (VIR)	2	1.0
266	NTNU University Museum	41	27.0
267	National Institute of Genetics, ROIS	1667	289.0
268	National Museum of Natural History, Smithsonian Institution	188897	20509.0
269	National Museum of Nature and Science, Japan	2080	638.0
270	Natural History Museum	16373	4684.0
271	Natural History Museum Rotterdam	345	177.0
272	Natural History Museum of Denmark	40	8.0
273	Natural History Museum of Los Angeles County	40525	1447.0
274	Natural History Museum of Utah (UMNH)	37	20.0
275	Natural History Museum, Vienna - Herbarium W	784	479.0
276	Natural History and Science Museum of the University of Porto (MHNC-UP)	201	65.0
277	Naturalis Biodiversity Center	43898	9656.0
278	Naturhistorisches Museum Basel - NMB	2	2.0
279	Naturhistorisches Museum Bern - NMBE	1	1.0
280	Naturhistorisches Museum Mainz	3	3.0
281	Naturkundemuseum im Ottoneum Kassel	11	11.0
282	New Brunswick Museum	68	16.0
283	New Mexico State Collection of Arthropods	37	20.0
284	New York State Museum (NYSM)	2	2.0
285	North Carolina State Museum of Natural Sciences	27	11.0
286	North Carolina State University Vascular Plant Herbarium (NCSC)	22	13.0
287	Northern Arizona University: Colorado Plateau Biodiversity Center	30	
288	Oleoducto Bicentenario	2074	93.0
289	Oregon State University	84	21.0
290	Organization for Tropical Studies	2	1.0
291	PANGAEA - Publishing Network for Geoscientific and Environmental Data	20668	91.0
292	Paleobiology Database	6720	754.0
293	Paleontological Research Institution	363	108.0
294	Parques Nacionales Naturales de Colombia	65070	3525.0
295	"Patrimonio Natural Fondo para la Biodiversidad y Áreas Protegidas ""Patrimonio Natural"""	7822	664.0
296	PhytoKeys	139	14.0
297	PlutoF	2112	237.0
298	Pontificia Universidad Javeriana	93946	6194.0
299	Pontifícia Universidade Católica do Rio Grande do Sul	46	23.0
300	Pontifícia Universidade Católica do Rio de Janeiro	54	49.0
301	Promigas S.A E.S.P	42789	840.0
302	Proyecto de Conservación de Aguas y Tierras - ProCAT Colombia	252	37.0
303	Queensland Museum	47	1.0
304	Questagame	210	68.0
305	Rancho Santa Ana Botanic Garden	104	68.0
306	Red Nacional de Jardines Botánicos de Colombia	208	76.0
307	Red Nacional de Observadores de Aves (RNOA)	424522	1644.0
308	Reef Life Survey	3867	196.0
309	Royal Belgian Institute of Natural Sciences	2822	546.0
310	Royal Botanic Garden Edinburgh	1278	850.0
311	Royal Botanic Gardens, Kew	5222	2899.0
312	Royal British Columbia Museum	26	12.0
313	Royal Ontario Museum	11053	995.0
314	Rutger's University - Chrysler Herbarium	1	1.0
315	SCAR - AntOBIS	1	1.0
316	SEAK S.A.S	155	123.0
317	Sam Noble Oklahoma Museum of Natural History	219	14.0
318	San Diego Natural History Museum	164	53.0
319	Santa Barbara Museum of Natural History	3	1.0
320	School of Forestry Engineering. Technical University of Madrid	1	1.0
321	Scripps Institution of Oceanography	733	236.0
322	Secretaría Distrital de Ambiente	33270	420.0
323	Senckenberg	6856	1793.0
324	Senckenberg Museum für Naturkunde Görlitz	7	2.0
325	Sistema de Informação sobre a Biodiversidade Brasileira - SiBBr	1	1.0
326	Société des Sciences Naturelles et Mathématiques de Cherbourg	7	3.0
327	South African National Biodiversity Institute	137	59.0
328	South Australian Museum	76	
329	Southeastern Louisiana University Vertebrate Museum	4	3.0
330	Southwestern Pacific Ocean Biogeographic Information System (OBIS) Node	17	5.0
331	Spanish National Museum of Natural Sciences (CSIC)	367	49.0
332	Staatliche Naturwissenschaftliche Sammlungen Bayerns	1876	624.0
333	Staatliches Museum für Naturkunde Karlsruhe	27	14.0
334	Staatliches Museum für Naturkunde Stuttgart	29	17.0
335	Stratos Consultoría Geológica	849	34.0
336	Swedish Museum of Natural History	2432	602.0
337	SysTax	522	290.0
338	T. L. Hankinson Vertebrate Museum, Eastern Michigan University	4	3.0
339	TELDAP	9	7.0
340	TERRASOS	9781	577.0
341	Taiwan Forestry Research Institute	7	4.0
342	Tall Timbers Research Station and Land Conservancy	1	1.0
343	Tasmanian Museum and Art Gallery	81	12.0
344	Tecnológico de Antioquia	7173	14.0
345	Tela Botanica	1	1.0
346	Texas A&M University Biodiversity Research and Teaching Collections	320	101.0
347	Texas A&M University Insect Collection	3663	56.0
348	The Genomic Observatories Metadatabase (GeOMe)	213	1.0
349	The National Institute of Water and Atmospheric Research (NIWA)	1362	201.0
350	The New York Botanical Garden	32238	7503.0
351	The Non-vertebrate Paleontology Laboratory, Jackson School Museum of Earth History, University of Texas at Austin	60	5.0
352	The South African Institute for Aquatic Biodiversity	3	3.0
353	The University Museum of Zoology, Cambridge	197	134.0
354	The University of the West Indies Zoology Museum (UWIZM)	23	9.0
355	Theodore  M. Sperry Herbarium	1	1.0
356	UCLA-Dickey Collection (UCLA-Dickey)	2	2.0
357	UNIBIO, IBUNAM	1472	536.0
358	US National Plant Germplasm System	1553	86.0
359	USDA-ARS Pollinating Insect-Biology, Management, Systematics Research	294	44.0
360	USF Water Institute	1438	837.0
361	UiO Natural History Museum	152	104.0
362	United Herbaria of the University and ETH Zurich (Z+ZT)	106	32.0
363	United States Geological Survey	285	82.0
364	Universidad CES	1138	85.0
365	Universidad Católica de Oriente	10547	1747.0
366	Universidad Central de Venezuela	620	50.0
367	Universidad Distrital Francisco José de Caldas	11460	2842.0
368	Universidad EAFIT	584	32.0
369	Universidad El Bosque	1932	336.0
370	Universidad Icesi	15531	1520.0
371	Universidad Industrial de Santander	38616	5009.0
372	Universidad Nacional de Colombia	475790	24954.0
373	Universidad Pedagógica y Tecnológica de Colombia	9795	1770.0
374	Universidad Pontificia Bolivariana	228	5.0
375	Universidad Tecnológica de Pereira	1894	412.0
376	Universidad Tecnológica del Chocó	15890	1946.0
377	Universidad de Antioquia	120192	9682.0
378	Universidad de Caldas	130557	4944.0
379	Universidad de Ciencias Aplicadas y Ambientales U.D.C.A	738	154.0
380	Universidad de Córdoba	2065	610.0
381	Universidad de Ibagué	246	4.0
382	Universidad de Nariño	22131	3279.0
383	Universidad de Pamplona	5053	786.0
384	Universidad de Salamanca. Nucleus	207	114.0
385	Universidad de Sucre	371	68.0
386	Universidad de la Amazonia	15430	1896.0
387	Universidad de la Salle	21202	2750.0
388	Universidad de los Andes	5191	615.0
389	Universidad de los Llanos	15824	910.0
390	Universidad del Cauca	709	107.0
391	Universidad del Magdalena	39568	411.0
392	Universidad del Quindío	16180	1031.0
393	Universidad del Sinú Cartagena	149	90.0
394	Universidad del Tolima	54780	3263.0
395	Universidad del Valle	96046	4235.0
396	Universidade Estadual Paulista - IBB	12	7.0
397	Universidade Estadual Paulista - Rio Claro	4	3.0
398	Universidade Estadual de Campinas - Instituto de Biologia	297	163.0
399	Universidade Estadual de Feira de Santana	200	86.0
400	Universidade Estadual de Londrina	1	1.0
401	Universidade Estadual de Londrina (UEL)	4	1.0
402	Universidade Federal da Bahia	4	4.0
403	Universidade Federal da Paraíba	43	5.0
404	Universidade Federal de Goiás	5	1.0
405	Universidade Federal de Minas Gerais	17	14.0
406	Universidade Federal de Pernambuco	49	39.0
407	Universidade Federal de Santa Catarina	2	2.0
408	Universidade Federal de Viçosa	2	
409	Universidade Federal do Paraná	193	73.0
410	Universidade Federal do Piauí	18	16.0
411	Universidade Federal do Rio Grande do Sul	59	43.0
412	Universidade Federal dos Vales do Jequitinhonha e Mucuri	2	2.0
413	Universidade de Brasília	46	39.0
414	Universidade de Caxias do Sul	12	12.0
415	Universidade de S達o Paulo	184	105.0
416	University of Alabama Biodiversity and Systematics	151	63.0
417	University of Alaska Museum of the North	9	4.0
418	University of Alberta Museums	46	31.0
419	University of Amsterdam / IBED	35752	1227.0
420	University of Arizona Insect Collection	10	5.0
421	University of Arizona Museum of Natural History	88	58.0
422	University of Arkansas	192	77.0
423	University of Bergen: University Museum	19	14.0
424	University of British Columbia	25	16.0
425	University of California Riverside	37	21.0
426	University of California, Davis	2	1.0
427	University of Colorado Museum of Natural History	2594	109.0
428	University of Connecticut	172	41.0
429	University of GdaÅ„sk, Dept. of Plant Taxonomy and Nature Conservation	88	24.0
430	University of Graz, Institute of Plant Sciences	33	29.0
431	University of Iowa Museum of Natural History	2	1.0
432	University of Kansas Biodiversity Institute	14280	873.0
433	University of Louisiana at Monroe	36	
434	University of Michigan Herbarium	1618	863.0
435	University of Michigan Museum of Zoology	5994	789.0
436	University of Minnesota Bell Museum	149	115.0
437	University of New Mexico Herbarium (UNM)	2	
438	University of North Carolina at Chapel Hill Herbarium (NCU)	265	164.0
439	University of Puerto Rico Mayagüez Invertebrate Collection	1	1.0
440	University of South Carolina	10	8.0
441	University of Tennessee - Chattanooga (UTC)	10	3.0
442	University of Tennessee Herbarium	99	67.0
443	University of Tennessee, Knoxville	1	1.0
444	University of Texas at Austin, Biodiversity Collections	642	67.0
445	University of Texas at El Paso Biodiversity Collections	345	90.0
446	University of Texas-Arlington	3248	242.0
447	University of Vienna - Herbarium WU	101	49.0
448	University of Washington Burke Museum	417	159.0
449	Université de Montréal Biodiversity Centre	163	148.0
450	Université de Namur Département de Biologie	96	54.0
451	Université de Strasbourg	1	1.0
452	Utah State University	48	25.0
453	Virginia Tech Insect Collection	4	1.0
454	WCS Colombia	19068	696.0
455	WWF Colombia	98	25.0
456	Walter Reed Biosystematics Unit, Smithsonian Institution	4254	167.0
457	West Virginia Wesleyan College	1	
458	Western Australian Museum	36	2.0
459	Western Foundation of Vertebrate Zoology	2302	574.0
460	Wooseokheon Natural History Museum	23	1.0
461	Worcester Society of Natural History d.b.a. EcoTarium	1	1.0
462	WrocÅ‚aw University, Museum of Natural History	3	1.0
463	Xeno-canto Foundation for Nature Sounds	17896	1262.0
464	Yale University Peabody Museum	2321	745.0
465	ZooKeys	6	1.0
466	Zoologic Audio, National Museum of Natural Science	28	11.0
467	Zoological Institute, Russian Academy of Sciences, St. Petersburg	7	3.0
468	Zoological Museum, Natural History Museum of Denmark	9	6.0
469	Zoologische Staatssammlung München/Staatliche Naturwissenschaftliche Sammlungen Bayerns	60	37.0
470	Zoologisches Forschungsinstitut und Museum Alexander Koenig	17	9.0
471	iNaturalist.org	56352	5930.0
472	naturgucker.de	2795	713.0
