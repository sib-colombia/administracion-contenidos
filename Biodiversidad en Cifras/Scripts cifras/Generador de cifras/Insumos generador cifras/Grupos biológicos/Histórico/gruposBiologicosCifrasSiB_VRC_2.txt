grupoBio	grupoTax	taxonRank	IPBES	COL	COL_1	COL_2	VRT_1	VRT_2	web	SIBM	comentarios	xCorregir
Moluscos	Mollusca	phylum	Aplica	Aplica	No Aplica	Aplica	No Aplica	Aplica	Aplica	Aplica		G2_col
Pinos y afines	Araucariaceae	family	No Aplica	Aplica	No Aplica	Aplica	No Aplica	Aplica	Aplica	No Aplica	De Clase Pinopsida se excluye la familia Podocarpaceae (familias restantes establecidas de acuerdo a catalog of life 2018)	
Pinos y afines	Cephalotaxaceae	family	No Aplica	Aplica	No Aplica	Aplica	No Aplica	Aplica	Aplica	No Aplica	De Clase Pinopsida se excluye la familia Podocarpaceae (familias restantes establecidas de acuerdo a catalog of life 2018)	
Pinos y afines	Cupressaceae	family	No Aplica	Aplica	No Aplica	Aplica	No Aplica	Aplica	Aplica	No Aplica	De Clase Pinopsida se excluye la familia Podocarpaceae (familias restantes establecidas de acuerdo a catalog of life 2018)	
Pinos y afines	Phyllocladaceae	family	No Aplica	Aplica	No Aplica	Aplica	No Aplica	Aplica	Aplica	No Aplica	De Clase Pinopsida se excluye la familia Podocarpaceae (familias restantes establecidas de acuerdo a catalog of life 2018)	
Pinos y afines	Pinaceae	family	No Aplica	Aplica	No Aplica	Aplica	No Aplica	Aplica	Aplica	No Aplica	De Clase Pinopsida se excluye la familia Podocarpaceae (familias restantes establecidas de acuerdo a catalog of life 2018)	
Pinos y afines	Sciadopityaceae	family	No Aplica	Aplica	No Aplica	Aplica	No Aplica	Aplica	Aplica	No Aplica	De Clase Pinopsida se excluye la familia Podocarpaceae (familias restantes establecidas de acuerdo a catalog of life 2018)	
Pinos y afines	Taxaceae	family	No Aplica	Aplica	No Aplica	Aplica	No Aplica	Aplica	Aplica	No Aplica	De Clase Pinopsida se excluye la familia Podocarpaceae (familias restantes establecidas de acuerdo a catalog of life 2018)	
Pinos y afines	Cycadaceae	family	No Aplica	Aplica	No Aplica	Aplica	No Aplica	Aplica	Aplica	No Aplica	De Clase Cycadopsida se excluye la familia Zamiaceae (familias restantes establecidas de acuerdo a catalog of life 2018)	
Pinos y afines	Gnetopsida	class	No Aplica	Aplica	No Aplica	Aplica	No Aplica	Aplica	Aplica	No Aplica		G2_col para evitar cruces entre grupos biológicos no excluyentes
