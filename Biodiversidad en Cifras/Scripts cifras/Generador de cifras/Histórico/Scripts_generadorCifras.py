# -*- coding: utf-8 -*-
"""
Created on Mon Oct 29 09:09:05 2018

@author: camila.plata
@author: iraida.barreto
Last modified 2019-03-04 
"""

import pandas as pd
import os

#EstablishName of geografic area to work With
#Be sure to also set g insede thegeographic loop
g='stateProvince' # choose between county (for regional data trendings) or stateProvince (for National data trendings)

def cal_registros(file):
    cur_path = os.getcwd()
    registros = pd.read_csv((cur_path + '\\' + file), sep=',')#leer cvs
    var =['threatStatus','migratory','endemic','exotic','appendixCITES']
    taxon = ['kingdom','phylum','class','order','family','genus','species'] # define una lista con todos los taxones
    i=0
    j=0
    rb_tax=pd.DataFrame()
    rb_the=pd.DataFrame()
    rb_tax_the=pd.DataFrame()
    rb_tax_cat=pd.DataFrame()
    
    for i in var:
        
        rb = registros.groupby(i)['gbifID'].count()
        rb_num = rb.to_frame(name = 'registros' ).reset_index()
        rb_num['thematic']=i
        rb_num=rb_num.rename(columns={i:'category'})
        rb_the=pd.concat([rb_the,rb_num])
        rb_the.to_csv('rb_the'+'.csv', sep=',', encoding = "ISO-8859-1")
        #rb_num.to_csv('rb_'+ i +'.csv', sep=',', encoding = "ISO-8859-1")
            
        
        for j in taxon:
            rb = registros.groupby(j)['gbifID'].count()
            rb_num = rb.to_frame(name = 'registros' ).reset_index()
            rb_num['taxonRank']=j
            rb_num=rb_num.rename(columns={j:'grupoTax'})
            rb_tax=pd.concat([rb_tax,rb_num])
            rb_tax.to_csv('rb_tax'+'.csv', sep=',', encoding = "ISO-8859-1")
                   
            rb = registros.groupby(j)[i].count()
            rb_num = rb.to_frame(name = 'registros' ).reset_index()
            rb_num['thematic']=i
            rb_num['taxonRank']=j
            rb_num=rb_num.rename(columns={j:'grupoTax'})
            rb_num=rb_num.rename(columns={i:'category'})
            rb_tax_the=pd.concat([rb_tax_the,rb_num])
            rb_tax_the.to_csv('rb_tax_the'+'.csv', sep=',', encoding = "ISO-8859-1")
            #rb_num.to_csv('rb_tot_'+ i +'_'+ j +'.csv', sep=',', encoding = "ISO-8859-1")

            rb = registros.groupby([j,i])[i].count()
            rb_num = rb.to_frame(name = 'registros' ).reset_index()
            rb_num['thematic']=i
            rb_num['taxonRank']=j
            rb_num=rb_num.rename(columns={j:'grupoTax'})
            rb_num=rb_num.rename(columns={i:'category'})
            rb_tax_cat=pd.concat([rb_tax_cat,rb_num])
            rb_tax_cat.to_csv('rb_tax_cat'+'.csv', sep=',', encoding = "ISO-8859-1")
            #rb_num.to_csv('rb_'+ i + '_' + j +'.csv', sep=',', encoding = "ISO-8859-1")

def cal_species(file):
    
    cur_path = os.getcwd()
    cvs_datos = pd.read_csv((cur_path + '\\' + file), sep=',')#leer cvs
    v01 = cvs_datos[cvs_datos['species'].notna()]#creamos una tabla que excluya las filas cuyo valor de especie sea nulo
    v02 = v01.drop_duplicates('species').sort_values(by=['species'])#valores únicos de especie  
    var =['threatStatus','migratory','endemic','exotic','appendixCITES']
    taxon = ['kingdom','phylum','class','order','family','genus','species'] # define una lista con todos los taxones
    i=0
    j=0
    sp_tax=pd.DataFrame()
    sp_the=pd.DataFrame()
    sp_tax_the=pd.DataFrame()
    sp_tax_cat=pd.DataFrame()

    for i in var:
        
        sp = v02.groupby(i)['gbifID'].count()
        sp_num = sp.to_frame(name = 'especies' ).reset_index()
        sp_num['thematic']=i
        sp_num=sp_num.rename(columns={i:'category'})
        sp_the=pd.concat([sp_the,sp_num])
        sp_the.to_csv('sp_the'+'.csv', sep=',', encoding = "ISO-8859-1")

        for j in taxon:
            sp = v02.groupby(j)['gbifID'].count()
            sp_num = sp.to_frame(name = 'especies' ).reset_index()
            sp_num['taxonRank']=j
            sp_num=sp_num.rename(columns={j:'grupoTax'})
            sp_tax=pd.concat([sp_tax,sp_num])
            sp_tax.to_csv('sp_tax'+'.csv', sep=',', encoding = "ISO-8859-1")
            
            sp = v02.groupby(j)[i].count()
            sp_num = sp.to_frame(name = 'especies' ).reset_index()
            sp_num['thematic']=i
            sp_num['taxonRank']=j
            sp_num=sp_num.rename(columns={j:'grupoTax'})
            sp_num=sp_num.rename(columns={i:'category'})
            sp_tax_the=pd.concat([sp_tax_the,sp_num])
            sp_tax_the.to_csv('sp_tax_the'+'.csv', sep=',', encoding = "ISO-8859-1")
                     
            sp = v02.groupby([j,i])[i].count()
            sp_num = sp.to_frame(name = 'especies' ).reset_index()
            sp_num['thematic']=i
            sp_num['taxonRank']=j
            sp_num=sp_num.rename(columns={j:'grupoTax'})
            sp_num=sp_num.rename(columns={i:'category'})
            sp_tax_cat=pd.concat([sp_tax_cat,sp_num])
            sp_tax_cat.to_csv('sp_tax_cat'+'.csv', sep=',', encoding = "ISO-8859-1")


    
def cal_geo(geo):
     
    cur_path = os.getcwd()
    geo = pd.read_csv((cur_path + '\\' + geo), sep=',').reset_index()
    var =['threatStatus','migratory','endemic','exotic','appendixCITES']
    i=0
    #EstablishName of geografic area to workWith
    g='county' # choose between county or stateProvince
    
    geo_the_rb=pd.DataFrame() # Y si esto fuera un data frame con todos los municipios de base y se generara un merge?
    geo_the_sp=pd.DataFrame()
    geo_cat_rb=pd.DataFrame()
    geo_cat_sp=pd.DataFrame()
    
    #Cifras registros por entidad geográfica
    geo_rb= geo.groupby(g)['gbifID'].count()
    rb= geo_rb.to_frame(name = 'registros').reset_index()
    rb.to_csv('rb_geo_tot'+'.csv', sep=',', encoding = "ISO-8859-1")
    
    #Cifras especies por entidad geográfica
    geo_sp= geo.groupby([g,'species'])['species'].count()
    sp= geo_sp.to_frame(name = 'registros').reset_index()
    sp= sp.groupby([g])['species'].count()
    sp= sp.to_frame(name = 'species').reset_index()
    sp.to_csv('sp_geo_tot'+'.csv', sep=',', encoding = "ISO-8859-1")
      
    for i in var:
    
       #Cifras registros  por entidad geográfica y total por temática
       geo_rb= geo.groupby([g,i])[i].count()
       rb= geo_rb.to_frame(name = 'registros').reset_index()
       rb['thematic']=i
       rb=rb.rename(columns={i:'category'})
       geo_cat_rb=pd.concat([geo_cat_rb,rb])
       geo_cat_rb.to_csv('geo_cat_rb' +'.csv', sep=',', encoding = "ISO-8859-1")
      
       #Conteo de especies por categorías de cada temática
       geo_sp= geo.groupby([g,i,'species']).size().reset_index()
       sp=geo_sp.groupby([g,i]).size()
       sp=sp.to_frame(name = 'especies').reset_index()
       sp['thematic']=i
       sp=sp.rename(columns={i:'category'})
       geo_cat_sp=pd.concat([geo_cat_sp,sp])
       geo_cat_sp.to_csv('geo_cat_sp' +'.csv', sep=',', encoding = "ISO-8859-1")
       
       #Conteo de total registros biológicos para todas las categorías de cada temática
       geo_rb = geo.groupby([g])[i].count()
       rb= geo_rb.to_frame(name = 'registros').reset_index()
       rb['thematic']=i
       rb=rb.rename(columns={i:'category'})
       geo_the_rb=pd.concat([geo_the_rb,rb])
       geo_the_rb.to_csv('geo_the_rb' +'.csv', sep=',', encoding = "ISO-8859-1")
          
       #Conteo de total especies únicas para todas las categorías cites
       geo_sp= geo_sp.groupby([g])[i].count()
       sp= geo_sp.to_frame(name = 'species').reset_index()
       sp['thematic']=i
       sp=sp.rename(columns={i:'category'})
       geo_the_sp=pd.concat([geo_the_sp,sp])
       geo_the_sp.to_csv('geo_the_sp' +'.csv', sep=',', encoding = "ISO-8859-1")

def read_cvs():
    #tomar el directorio y el archivo
    print ('CONTEO DE REGISTROS')
    file = "boyaca_datos.csv"
    cur_path = os.getcwd()
    print ('directorio actual del archivo para conteo de especies:' + cur_path +'\\' + file) 
    cal_registros(file)
    
    print ('CONTEO DE ESPECIES')
    file = "boyaca_datos.csv"
    cur_path = os.getcwd()
    print ('directorio actual del archivo para conteo de especies:' + cur_path +'\\' + file) 
    cal_species(file)
    
    print ('\n CONTEO GEOGRÁFICO')
    geo = "boyaca_datos.csv"
    cur_path = os.getcwd()             
    print ('directorio actual del archivo para conteo geográfico:' + cur_path +'\\' + geo)
    cal_geo(geo)

read_cvs()

rb= pd.read_csv('rb_tax.csv',encoding='latin-1') 
#improve with ignoring first colum, so there is no need to specify column by column
#to implement in the remaining file
rb=rb.drop(rb.columns[0],axis=1) 
sp= pd.read_csv('sp_tax.csv',usecols=['especies','grupoTax','taxonRank'],encoding='latin-1')
gbio=pd.read_table('gruposBiologicos.txt')

rb_gb=pd.merge(rb,gbio, on=['grupoTax','taxonRank'],how='left') #genera duplicados
rb_gb=rb_gb.drop_duplicates()
rb_gb=rb_gb[['registros','grupoBio']]
rb_cifras=rb_gb.groupby('grupoBio').sum().reset_index()

sp_gb=pd.merge(sp,gbio, on=['grupoTax','taxonRank'],how='left')
sp_gb=sp_gb.drop_duplicates()
sp_gb=sp_gb[['especies','grupoBio']]
sp_cifras=sp_gb.groupby('grupoBio').sum().reset_index()


tax_cifras=pd.merge(rb_cifras,sp_cifras, on='grupoBio',how='left') #file to use

#Getting total rb and sp counts per biological group per thematic
rb_tax_the= pd.read_csv('rb_tax_the.csv',usecols=['registros','grupoTax','taxonRank','thematic'],encoding='latin-1')
sp_tax_the= pd.read_csv('sp_tax_the.csv',usecols=['especies','grupoTax','taxonRank','thematic'],encoding='latin-1')

rb_tax_the_gb=pd.merge(rb_tax_the,gbio, on=['grupoTax','taxonRank'],how='left')
rb_tax_the_gb=rb_tax_the_gb.drop_duplicates()
rb_tax_the_gb=rb_tax_the_gb[['registros','grupoBio','thematic']]
rb_tax_the_cifras=rb_tax_the_gb.groupby(['grupoBio','thematic']).sum().reset_index()

sp_tax_the_gb=pd.merge(sp_tax_the,gbio, on=['grupoTax','taxonRank'],how='left')
sp_tax_the_gb=sp_tax_the_gb.drop_duplicates()
sp_tax_the_gb=sp_tax_the_gb[['especies','grupoBio','thematic']]
sp_tax_the_cifras=sp_tax_the_gb.groupby(['grupoBio','thematic']).sum().reset_index()

tax_the_cifras=pd.merge(rb_tax_the_cifras,sp_tax_the_cifras, on=['grupoBio','thematic'],how='left')
tax_the_cifras=tax_the_cifras.pivot('grupoBio','thematic').fillna(0) #file to use

#Getting total rb and sp counts per biological group per category
rb_tax_cat= pd.read_csv('rb_tax_cat.csv',usecols=['registros','grupoTax','taxonRank','thematic','category'],encoding='latin-1')
sp_tax_cat= pd.read_csv('sp_tax_cat.csv',usecols=['especies','grupoTax','taxonRank','thematic','category'],encoding='latin-1')

rb_tax_cat_gb=pd.merge(rb_tax_cat,gbio, on=['grupoTax','taxonRank'],how='left')
rb_tax_cat_gb=rb_tax_cat_gb.drop_duplicates()
rb_tax_cat_gb=rb_tax_cat_gb[['registros','grupoBio','thematic','category']]
rb_tax_cat_cifras=rb_tax_cat_gb.groupby(['grupoBio','thematic','category']).sum().reset_index()

sp_tax_cat_gb=pd.merge(sp_tax_cat,gbio, on=['grupoTax','taxonRank'],how='left')
sp_tax_cat_gb=sp_tax_cat_gb.drop_duplicates()
sp_tax_cat_gb=sp_tax_cat_gb[['especies','grupoBio','thematic','category']]
sp_tax_cat_cifras=sp_tax_cat_gb.groupby(['grupoBio','thematic','category']).sum().reset_index()

tax_cat_cifras=pd.merge(rb_tax_cat_cifras,sp_tax_cat_cifras, on=['grupoBio','thematic','category'],how='left') 
tax_cat_cifras=tax_cat_cifras.drop(['thematic'],axis=1)
tax_cat_cifras=tax_cat_cifras.pivot('grupoBio','category').fillna(0) #file to use #acá es donde salen desorganizadas las categorias

gBio_tot=pd.merge(tax_cifras,tax_the_cifras,on='grupoBio',how='left').merge(tax_cat_cifras,on='grupoBio',how='left') ###FinalBiologicalGroupFile


#Renaming colummns
gBio_tot.columns=["grupoBio",
"registros",
"especies",
"registrosCites",
"registrosEndemicas",
"registrosExoticas",
"registrosMigratorias",
"registrosAmenaza",
"especiesCites",
"especiesEndemicas",
"especiesExoticas",
"especiesMigratorias",
"especiesAmenaza",
"registrosCR",
"registrosEN",
"registrosEndémica",
"registrosExótica",
"registrosCitesI",
"registrosCitesII",
"registrosCitesIII",
"registrosMigratoria",
"registrosVU",
"especiesCR",
"especiesEN",
"especiesEndémica",
"especiesExótica",
"especiesCitesI",
"especiesCitesII",
"especiesCitesIII",
"especiesMigratoria",
"especiesVU"
]

#Drop unnecesary collumns
gBio_tot=gBio_tot.drop(["especiesEndémica","especiesExótica","especiesMigratoria",
               "registrosEndémica","registrosExótica","registrosMigratoria"], axis=1)

#Re-organice collumns
gBio_tot=gBio_tot[["grupoBio","especies","registros","especiesAmenaza","especiesVU","especiesEN",
                   "especiesCR","registrosAmenaza","registrosVU","registrosEN","registrosCR","especiesCites",
                   "especiesCitesI","especiesCitesII","especiesCitesIII","registrosCites",
                   "registrosCitesI","registrosCitesII","registrosCitesIII","especiesEndemicas",
                   "especiesExoticas","especiesMigratorias","registrosEndemicas","registrosExoticas","registrosMigratorias"]]

gBio_tot.to_csv('gBio.txt',sep='\t')

#Merging total rb and sp per thematic x category
rb_the= pd.read_csv('rb_the.csv',usecols=['registros','thematic','category'],encoding='latin-1')
sp_the= pd.read_csv('sp_the.csv',usecols=['especies','thematic','category'],encoding='latin-1')

the_cifras=pd.merge(rb_the,sp_the, on=['thematic','category'],how='left') ##### file to use total count per category????

the_cifras.to_csv('the_cifras.txt',sep='\t')
##########
#Merging geographical counts

#Merging total rb and sp counts per county
rb_geo_tot= pd.read_csv('rb_geo_tot.csv',encoding='latin-1')
rb_geo_tot=rb_geo_tot.drop(rb_geo_tot.columns[0],axis=1)
sp_geo_tot= pd.read_csv('sp_geo_tot.csv',encoding='latin-1')
sp_geo_tot=sp_geo_tot.drop(sp_geo_tot.columns[0],axis=1)

geo_tot=pd.merge(rb_geo_tot,sp_geo_tot, on=g, how='left')

geo_the_rb= pd.read_csv('geo_the_rb.csv',encoding='latin-1')
geo_the_rb=geo_the_rb.drop(geo_the_rb.columns[0],axis=1) 
geo_the_rb=geo_the_rb.pivot(g,'thematic').fillna(0) # pivot data frame to get thematic as headers

geo_the_sp= pd.read_csv('geo_the_sp.csv',encoding='latin-1') #encoding='latin-1' otherwise get a decoding error due to accents
geo_the_sp=geo_the_sp.drop(geo_the_sp.columns[0],axis=1)
geo_the_sp=geo_the_sp.pivot(g,'thematic').fillna(0)

geo_cat_rb= pd.read_csv('geo_cat_rb.csv',encoding='latin-1')
geo_cat_rb=geo_cat_rb.drop(geo_cat_rb.columns[0],axis=1)
geo_cat_rb=geo_cat_rb.drop(geo_cat_rb.columns[3],axis=1)
geo_cat_rb=geo_cat_rb.pivot(g,'category').fillna(0)

geo_cat_sp= pd.read_csv('geo_cat_sp.csv',encoding='latin-1') #encoding='latin-1' otherwise get a decoding error due to accents
geo_cat_sp=geo_cat_sp.drop(geo_cat_sp.columns[0],axis=1) 
geo_cat_sp=geo_cat_sp.drop(geo_cat_sp.columns[3],axis=1)
geo_cat_sp=geo_cat_sp.pivot(g,'category').fillna(0)

geo_tot=pd.merge(rb_geo_tot,sp_geo_tot,on=g,how='left').merge(geo_the_rb,on=g,how='left').merge(geo_the_sp,on=g,how='left')
geo_tot=pd.merge(geo_tot,geo_cat_rb,on=g,how='left').merge(geo_cat_sp,on=g,how='left') #final file to use

geo_tot.to_csv('geo_tot.txt',sep='\t')
