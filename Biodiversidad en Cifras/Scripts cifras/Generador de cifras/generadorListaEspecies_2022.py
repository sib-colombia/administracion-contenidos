'''
Created: 2021-07-07
Last update: 2021-10-07

@author: Nerieth Leuro


Este scipt está diseñado para obtener el listado de todas las especies reportadas para el departamento y municipios correspondiente.
Lista el nombre de la especie relacionados al municipio y departamento donde se encuentran registrados. Adicionalmente, se encuentran 
las diferentes temáticas y categorías que apliquen, el número de registros y en el caso de que se cuente con la información si la especie 
corresponde a hábitat marino, continental o salobre y el número de registros correspondientes a cada uno de estos. 
El archivo que se utilice para la ejecución de este script debe haber pasado por un proceso de validación, limpieza 
y una revisión del equipo de trabajo para asegurar la presencia de los campos necesarios. 
Adicionalmente, se debe contar con los archivos complementarios, correspondientes a DIVIPOLA en el caso de que se deban aplicar validaciones
para el campo de stateProvince.

Al terminar la ejecución del script se obtendrá un archivo con toda la información relacionada anteriormente.

Tenga en cuenta que dentro del script podra encontrar algunas secciones identificadas con ##, estas corresponden a líneas explicativas. 
Y partes del código que se encuentran comentariadas con # que corresponden a secciones de código que se pueden habilitar o deshabilitar
según los datos que se deseen obtener.

'''
##-----------------------------------------------------Preparar el entorno---------------------------------------------##
##Importar las librerías necesarias para el uso del script
from numpy.core.numeric import NaN
import pandas as pd 
import os 
import time
import numpy as np


##Iniciar el conteo de tiempo de ejecución del script
inicio=time.time()

##Establecer la carpeta de trabajo
os.chdir("D:\cifras_Nariño\Versión 2021")

##Crear una variable string con el nombre del conjunto de datos. Este nombre debe incluir: la geografía, el año y el corte trimestral utilizado 
nombre='ListaEspeciesNariñoT3'

##----------------------------------------------------Cargar archivos--------------------------------------------------##
'''
Para conjuntos de datos de gran tamaño y equipos con poca memoria RAM (8 gb o menos)
Se agrega dtype (asignar el tipo de dato a cada columna)
Con el fin de evitar el error: pandas.errors.ParserError: Error tokenizing data. C error: out of memory

dtype: se especifica cada columna y el tipo de dato que corresponde, el orden de los campos no afecta la ejecución, pero debe actualizarse
cada que se agreguen y quiten campos para hacer más eficiente el proceso.
    str: Se utiliza para campos que contengan texto y símbolos
    float: Aplica en campos que contengan números decimales
 
'''
##Crear un objeto cargando la tabla de datos(formato csv)
registros = pd.read_table('Narino_data2021T3_finales.csv', sep=',', encoding = "utf8",usecols=['gbifID', 'organization','publishingCountry','logoUrl','county', 'stateProvince','municipality',
'scientificName','species', 'kingdom', 'phylum', 'class', 'order', 'family', 'genus', 'taxonRank','threatStatus_UICN', 'isInvasive_mads', 'appendixCITES', 
 'threatStatus_MADS', 'exotic', 'isInvasive_griis', 'endemic', 'migratory', 'Municipio-ubicacionCoordenada','Departamento-ubicacionCoordenada',
 'isMarine','isBrackish','isTerrestrial', 'ZonaMaritima'],dtype=str)   

##Cargar archivo de referencia geográfica 
staProv_divipola = pd.read_table('DIVIPOLA_20200311_unique.txt', encoding = "utf8")

##Crear un objeto cargando un archivo de texto (formato txt)
#registros = pd.read_table('Narino_data2021F23.txt',sep='\t',encoding = 'utf8' )
'''
La variable 'tipo' permite condicionar los procesos teniendo en cuenta si se van a sacar cifras departamentales o municipales y si 
el conjunto de datos contiene información de registros marítimos
Departamental con datos marinos='DCDM'
Departamental sin datos marinos='DSDM'
Municipal con datos marinos='MCDM'
Municipal sin datos marinos='MSDM'

En los casos de Departamental sin datos marinos y Municipal sin datos marinos:
Se muestra el listado de especies, la geografía correspondiente, el número registros y las categorías temáticas que apliquen.

En los casos de Departamental con datos marinos y Municipal con datos marinos:
Se obtiene el listado de especies, la geografía correspondiente, el número registros y las categorías temáticas que apliquen a nivel general
y discriminadas por hábitat marino, terrestre y salobre.


Es importante aclarar que debido a las hábitos y distribución de las especies se pueden encontrar especies presentes en más de un hábitat
por lo tanto, el valor general no corresponde a la suma de los valores para cada hábitat

'''
#tipo='DCDM'
#tipo='DSDM'
tipo='MCDM'
#tipo='MSDM'

## Definir el alcance de las cifras geográficas
## Seleccionar 'stateProvince' para cifras Nacionales, 'county' para cifras departamentales
if tipo =='MCDM' or tipo =='MSDM':
    geografia='county' 
if tipo =='DCDM' or tipo =='DSDM':
    geografia='stateProvince' 

##--------------------------------------------Ajustes preliminares de los datos ---------------------------------------------##
'''
Cuando se realiza la ejecución de este script sin realizar un proceso de limpieza en el campo de stateProvince se debe realizar el cruce con el
archivo de referencia geográfica DIVIPOLA. Lo cual permitirá asegurar que en este campo solo se encuentren los departamentos válidos para el
país.
'''

##Listar las columnas del conjunto de datos
#list(registros.columns) 

##Reajustar campo stateProvince (asignar al campo verbatimStateProvince el valor de stateProvince)
registros['verbatimStateProvince']=registros['stateProvince']

##Quitar comillas
registros['stateProvince']=registros.stateProvince.str.replace('"','')

## Unir las tablas por medio del campo stateProvince
registros=pd.merge(registros,staProv_divipola, on='stateProvince',how='left') 

#Reemplazar los valores null de stateProvinceDivipola por los valores de Departamento-ubicacionCoordenada
registros['stateProvince']=registros['stateProvince_divipola'].mask(pd.isnull, registros['Departamento-ubicacionCoordenada'])


##Ajuste de las categorías threathStatus para asegurar integridad de las cifras MADS e UICN en los conteos Departamento-ubicacionCoordenada
registros.threatStatus_UICN=registros.threatStatus_UICN + '_UICN'
registros.threatStatus_MADS=registros.threatStatus_MADS + '_MADS'

##SubsetTaxonómico si se necesitan cifras totales por reinos (obtener solo los datos que pertenezcan a ese reino)
#registros= registros[(registros['kingdom'] =='Animalia')]

##Obtener el número de registros para cada reino
#numSpEndemicas=registros.groupby(["kingdom"])["kingdom"].count()

##Condicional para registros marinos, continentales y salobres

if tipo =='MCDM' or tipo =='DCDM':
    registros.loc[(registros['stateProvince'].isin(['Amazonas','Bogotá, D.C.','Caquetá','Cundinamarca'
    ,'Huila','Putumayo','Santander','Vaupés','Arauca','Boyacá','Casanare', 'Guainía','Meta','Quindío',
    'Tolima','Vichada','Caldas','Cesar','Guaviare','Norte de Santander','Risaralda'])), 'isMarine'] = np.nan

##-------------------------------------------------Loop calculo cifras ---------------------------------------------------##
'''
El loop permite iterar en el conjunto de datos, realizando diferentes procesos y creación de variables, teniendo en cuenta 
las categorias tematicas y los grupos taxonómicos 

Dentro de los nombres se encuentran las siguientes convenciones:
rb= Registros biologicos
sp= especies
geo= geografía
'''

## Crear listas con las categorías temáticas sobre las cuales se corre el loop
tematica =['threatStatus_UICN','isInvasive_mads','appendixCITES','threatStatus_MADS','exotic','isInvasive_griis','endemic','migratory']

## Define una lista con todos las categorias taxonomicas
#taxon = ['kingdom','phylum','class','order','family','genus','species'] 

## Creación de dataframes vacíos para guardar la información de la lista
rb_numero_total=pd.DataFrame()
rb_numero_habitat=pd.DataFrame()
rb_numero_habitat_total=pd.DataFrame()
sp_rb_categoria=pd.DataFrame()
sp_rb_categoria_total=pd.DataFrame()

rb_numero_continental=pd.DataFrame()

##Esta variable almacena el conteo de registros total agrupado por geografía y taxon, incluyendo los que no se encuentran
##en ninguna categoría de interés


if tipo =='MCDM' or tipo =='MSDM':
    rb_numero_total= registros.groupby([geografia,'stateProvince','species'], dropna=False)['gbifID'].count().to_frame(name = 'registros').reset_index()
if tipo =='DCDM' or tipo =='DSDM':
    rb_numero_total= registros.groupby([geografia,'species'], dropna=False)['gbifID'].count().to_frame(name = 'registros').reset_index()
rb_numero_total = rb_numero_total[rb_numero_total['species'].notna()]


##Condicional para registros marinos, continentales y salobres
if tipo =='MCDM':
    ##En las variables rb_numero_marino, rb_numero_continental, rb_numero_salobre se almacenan los registros que corresponden a cada
    ## hábitat, agrupados por geografía y taxon
    rb_numero_marino= registros.groupby([geografia,'stateProvince','species','isMarine'])['gbifID'].count().to_frame(name = 'registrosMarinos').reset_index()                  
    rb_numero_marino = rb_numero_marino[rb_numero_marino['isMarine'].notna()]   
    rb_numero_continental= registros.groupby([geografia,'stateProvince','species','isTerrestrial'])['gbifID'].count().to_frame(name = 'registrosContinentales').reset_index()                           
    rb_numero_continental = rb_numero_continental[rb_numero_continental['isTerrestrial'].notna()]   
    rb_numero_salobre= registros.groupby([geografia,'stateProvince','species','isBrackish'])['gbifID'].count().to_frame(name = 'registrosSalobres').reset_index()                  
    rb_numero_salobre = rb_numero_salobre[rb_numero_salobre['isBrackish'].notna()]   
    
    ##Se unen los registros obtenidos para cada uno de los habitats           
    rb_numero_habitat=pd.merge(rb_numero_total,rb_numero_continental, on=[geografia,'species','stateProvince'],how='left').merge(rb_numero_marino, on=[geografia,'species','stateProvince'],how='left').merge(rb_numero_salobre, on=[geografia,'species','stateProvince'],how='left')  
    rb_numero_habitat = rb_numero_habitat[rb_numero_habitat['species'].notna()]
    rb_numero_habitat_total=pd.concat([rb_numero_habitat_total,rb_numero_habitat])

if tipo =='DCDM':   
    rb_numero_marino= registros.groupby(['stateProvince','species','isMarine'], dropna=False)['gbifID'].count().to_frame(name = 'registrosMarinos').reset_index()                  
    rb_numero_marino = rb_numero_marino[rb_numero_marino['isMarine'].notna()]   
    rb_numero_continental= registros.groupby(['stateProvince','species','isTerrestrial'], dropna=False)['gbifID'].count().to_frame(name = 'registrosContinentales').reset_index()                   
    rb_numero_continental = rb_numero_continental[rb_numero_continental['isTerrestrial'].notna()]   
    rb_numero_salobre= registros.groupby(['stateProvince','species','isBrackish'], dropna=False)['gbifID'].count().to_frame(name = 'registrosSalobres').reset_index()                  
    rb_numero_salobre = rb_numero_salobre[rb_numero_salobre['isBrackish'].notna()]   
    ##Se unen los registros obtenidos para cada uno de los habitats           
    rb_numero_habitat=pd.merge(rb_numero_total,rb_numero_continental, on=[geografia,'species'],how='left').merge(rb_numero_marino, on=[geografia,'species'],how='left').merge(rb_numero_salobre, on=[geografia,'species'],how='left')
    rb_numero_habitat = rb_numero_habitat[rb_numero_habitat['species'].notna()]   
    rb_numero_habitat_total=pd.concat([rb_numero_habitat_total,rb_numero_habitat])
 
   
for i in tematica:                       
    ##Se crea una variable que almacena las especies y la temática de interés
    if tipo =='MCDM' or tipo =='MSDM':
        rb_numero_categoria = registros.groupby(['species',i,geografia,'stateProvince'], dropna=False)[i].count().to_frame(name = 'registros' ).reset_index()
    if tipo =='DCDM' or tipo =='DSDM':
        rb_numero_categoria = registros.groupby(['species',i,'stateProvince'], dropna=False)[i].count().to_frame(name = 'registros' ).reset_index()
    rb_numero_categoria['thematic']=i
    rb_numero_categoria = rb_numero_categoria[rb_numero_categoria['species'].notna()]

    ##Se valida cada una de las temáticas y se ajustan los nombres generales
    rb_numero_categoria.loc[rb_numero_categoria.thematic=='threatStatus_UICN','thematic']='Amenaza IUCN'
    rb_numero_categoria.loc[rb_numero_categoria.thematic=='threatStatus_MADS','thematic']='Amenaza MADS'
    rb_numero_categoria.loc[rb_numero_categoria.thematic=='appendixCITES','thematic']='CITES'
    
    if i=='exotic'or i=='isInvasive_mads' or i=='migratory' or i=='isInvasive_griis' or i=='endemic':
        rb_numero_categoria['thematic']='Distribución'
    rb_numero_categoria=rb_numero_categoria.rename(columns={i:'category'})
  
    ##Se unen el conjunto de datos general (cuando se ejecuta sin la información de hábitat) y las temáticas
    if tipo =='MCDM' or tipo =='MSDM':
        sp_rb_categoria=pd.merge(rb_numero_total,rb_numero_categoria, on=[geografia,'species','stateProvince','registros'],how='left')
    if tipo =='DCDM' or tipo =='DSDM':
        sp_rb_categoria=pd.merge(rb_numero_total,rb_numero_categoria, on=['species','stateProvince','registros'],how='left')
  
    sp_rb_categoria_total=pd.concat([sp_rb_categoria_total,sp_rb_categoria])
  

##Condicional para registros marinos, continentales y salobres
if tipo =='MCDM':
    ##Se une el conjunto de datos con la información de temáticas y los que contienen cada hábitat
    geo_sp_categoria_total=pd.merge(sp_rb_categoria_total,rb_numero_habitat_total, on=[geografia,'species','stateProvince','registros'],how='left')     
if tipo =='DCDM':   
    geo_sp_categoria_total=pd.merge(sp_rb_categoria_total,rb_numero_habitat_total, on=['species','stateProvince','registros'],how='left')
if tipo =='DSDM' or tipo =='MSDM':  
    geo_sp_categoria_total=sp_rb_categoria_total

##Se eliminan duplicados y se terminan de ajustar los nombres de todo el conjunto de datos
geo_sp_categoria_total = geo_sp_categoria_total.drop_duplicates()
geo_sp_categoria_total=geo_sp_categoria_total.replace('_UICN',' UICN',regex=True)           
geo_sp_categoria_total=geo_sp_categoria_total.replace('_MADS',' MADS',regex=True)    
geo_sp_categoria_total=geo_sp_categoria_total.replace(' GRIIS','',regex=True)
geo_sp_categoria_total=geo_sp_categoria_total.replace('Marine','marino',regex=True)
geo_sp_categoria_total=geo_sp_categoria_total.replace('Brackish','salobre',regex=True)
geo_sp_categoria_total=geo_sp_categoria_total.replace('Terrestrial','continental',regex=True)

geo_sp_categoria_total=geo_sp_categoria_total.rename(columns={'isTerrestrial':'continental'})
geo_sp_categoria_total=geo_sp_categoria_total.rename(columns={'isMarine':'marino'})
geo_sp_categoria_total=geo_sp_categoria_total.rename(columns={'isBrackish':'salobre'})
 
if tipo =='MCDM':
     geo_sp_categoria_total=geo_sp_categoria_total[['species',geografia,'stateProvince','thematic','category','registros','continental','registrosContinentales','marino','registrosMarinos','salobre','registrosSalobres']]
     #geo_sp_categoria_total=geo_sp_categoria_total[['species',geografia,'stateProvince','thematic','category','registros','isTerrestrial','registrosContinentales','isMarine','registrosMarinos','isBrackish','registrosSalobres']]
if  tipo =='MSDM':
     geo_sp_categoria_total=geo_sp_categoria_total[['species',geografia,'stateProvince','thematic','category','registros']]

if tipo =='DCDM':
     geo_sp_categoria_total=geo_sp_categoria_total[['species','stateProvince','thematic','category','registros','continental','registrosContinentales','marino','registrosMarinos','salobre','registrosSalobres']]
if tipo =='DSDM':
    geo_sp_categoria_total=geo_sp_categoria_total[['species','stateProvince','thematic','category','registros']]
#gsct=geo_sp_categoria_total[geo_sp_categoria_total['registros']==0].index
#geo_sp_categoria_total=geo_sp_categoria_total.drop(gsct)


geo_sp_categoria_total_drop=geo_sp_categoria_total[geo_sp_categoria_total["registros"]==0].index
geo_sp_categoria_total=geo_sp_categoria_total.drop(geo_sp_categoria_total_drop)

##crear archivo csv 
geo_sp_categoria_total.to_csv(nombre+'.tsv',sep='\t', index=False)     
##crear archivo excel y la hoja Cifras totales
geo_sp_categoria_total.to_excel(nombre +'.xlsx', sheet_name='Lista especies', index=False)         

fin=time.time()    
print(fin-inicio)
