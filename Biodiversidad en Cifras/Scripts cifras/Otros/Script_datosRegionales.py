# -*- coding: utf-8 -*-
"""
Created on Fri Mar  6 15:28:31 2020

@author: ricardo.ortiz
"""


import pandas as pd


dwc_co= pd.read_table("C:\\Cifras_Colombia\\2020\\Datos_Trimestrales\\TrimestreIV\\dwc_co_20201231.txt", encoding = "utf8", sep= "\t")

#Revisar los valores únicos documentados en stateProvince para el consolidado de datos trimestral
dwc_co['stateProvince'].unique()
#Generar un dataset de valores únicos para explorar opciones (OpenRefine)
stateProvince = dwc_co.groupby(['stateProvince']).gbifID.nunique().reset_index()
county = dwc_co.groupby(['county']).gbifID.nunique().reset_index()

stateProvince.to_csv('C:\\Cifras_Colombia\\2021\Regionales\\GroupBy_stateProvince.csv', sep="\t", encoding = "utf8")
county.to_csv('C:\\Cifras_Colombia\\2021\Regionales\\GroupBy_county.csv', sep="\t", encoding = "utf8")


#Departamentos opciones

Boyaca = 'Boyacá','Boy','Boyac','BOYAC??','Boyaca', 'Departamento de Boyac'
Santander = 'Santander','Santander del Sur','Santander del sur','SANTANDER/N. Y CESAR'
Narino = '[NARINO]','Just east of Rio Espinayaco, 21.6 km east of border with Narino Department, 1.8 km west of town square of Santiago.','NARI??O','Narino','Narino-Ipiales road to La Victoria, 3 to 8 km along the way to Verada La Estrella.','nariño','Nariño','Nario'
Huila = '^Huila','At km 45.6 from Popayan to Paletara, through San Jose de Isnos in Department El Huila.','Huila'
Tolima = '27 km from La Florida to Herrera Tolima, passing through La Diana. At Alto de Monserrate in La Ermita.','At Paramo Cerro Pelado, 48 km from La Florida along a new road to Herrera and El Tolima.','At Paramo of Cerro Pelado near Laguna Los Micos, 46.6 km from La Florida (near Cali) to Laguna Las Tinajas and Hermosa on new road to Herrera El Tolima.','Cundinamarca / Tolima','Road towards El Campenario, between Tolima and Quindio Departments limits.','Tolima'
Bogota = '"Bogota, D.C."', 'Andes prs de Bogot; San Baelazar','Bogot','Bogota','BOGOTÁ D.C.','Bogota market','BOGOTÁ, D.C.','Bogotá, D.C.','Chiquinquiva, pres de Bogota.','Distrito Capitl de Santa F de Bogot','From the market at Bogota, Columbia.','La Uribe near Bogota','Prs Bogot','Prs Bogot; San Cristobl','Cundinamarca / Distrito Capital','Distrito Capital','Distrito capital','Distrito Capitl de Santa F de Bogot'
Cundinamarca = '^Cundinamarca','CunDenmark','Cundimarca','Cundinamarca','Cundinamarca / Distrito Capital','Cundinamarca / Tolima','Cundinarmarca','Departamento de Cundinamarca',

#Santander

dpto_t = dwc_co[dwc_co['stateProvince'].isin(Santander)]
dpto_g = dwc_co[dwc_co['Departamento-ubicacionCoordenada'].isin(Santander)]
Santander_data=pd.concat([dpto_g,dpto_t], sort=False)
Santander_data=Santander_data.drop_duplicates('gbifID',inplace=False)


#Boyaca

dpto_t = dwc_co[dwc_co['stateProvince'].isin(Boyaca)]
dpto_g = dwc_co[dwc_co['Departamento-ubicacionCoordenada'].isin(Boyaca)]
Boyaca_data=pd.concat([dpto_g,dpto_t], sort=False)
Boyaca_data=Boyaca_data.drop_duplicates('gbifID',inplace=False)


#Nariño

dpto_t = dwc_co[dwc_co['stateProvince'].isin(Narino)]
dpto_g = dwc_co[dwc_co['Departamento-ubicacionCoordenada'].isin(Narino)]
Narino_data=pd.concat([dpto_g,dpto_t], sort=False)
Narino_data=Narino_data.drop_duplicates('gbifID',inplace=False)


#Regional 
Regional_data=pd.concat([Santander_data,Boyaca_data,Narino_data], sort=False)#1066367
Regional_data=Regional_data.drop_duplicates('gbifID',inplace=False) #1053713

#Bogotá

BogotaCounty = '"Bogota, D.C."','Bogot','Bogot D.C','Bogot D.C.','Bogota','Bogotá','BOGOTÁ','BOGOTÁ D.C.','Bogotá D.C.','BOGOTÁ, D.C.','Bogotá, D.C.','El Humbo region, I30 M-N of Bogota','Macizo de Bogot',
dpto_t = dwc_co[dwc_co['stateProvince'].isin(Tolima)]
dpto_g = dwc_co[dwc_co['Departamento-ubicacionCoordenada'].isin(Tolima)]
Tolima_data=pd.concat([dpto_g,dpto_t], sort=False)
Tolima_data=Tolima_data.drop_duplicates('gbifID',inplace=False)

dpto_t = dwc_co[dwc_co['stateProvince'].isin(Bogota)]
dpto_g = dwc_co[dwc_co['Departamento-ubicacionCoordenada'].isin(Bogota)]
mpio_t = dwc_co[dwc_co['county'].isin(BogotaCounty)]
mpio_g = dwc_co[dwc_co['Municipio-ubicacionCoordenada'].isin(BogotaCounty)]
Bogota_data=pd.concat([dpto_g,dpto_t,mpio_t,mpio_g], sort=False)
Bogota_data=Bogota_data.drop_duplicates('gbifID',inplace=False)


#Tolima

dpto_t = dwc_co[dwc_co['stateProvince'].isin(Tolima)]
dpto_g = dwc_co[dwc_co['Departamento-ubicacionCoordenada'].isin(Tolima)]
Tolima_data=pd.concat([dpto_g,dpto_t], sort=False)
Tolima_data=Tolima_data.drop_duplicates('gbifID',inplace=False)


#Huila

dpto_t = dwc_co[dwc_co['stateProvince'].isin(Huila)]
dpto_g = dwc_co[dwc_co['Departamento-ubicacionCoordenada'].isin(Huila)]
Huila_data=pd.concat([dpto_g,dpto_t], sort=False)
Huila_data=Huila_data.drop_duplicates('gbifID',inplace=False)



#Exportar
Santander_data.to_csv('C:\\Cifras_Colombia\\2021\Regionales\\Santander_data.txt', sep="\t", encoding = "utf8")
Boyaca_data.to_csv('C:\\Cifras_Colombia\\2021\Regionales\\Boyaca_data.txt', sep="\t", encoding = "utf8")
Narino_data.to_csv('C:\\Cifras_Colombia\\2021\Regionales\\Narino_data.txt', sep="\t", encoding = "utf8")
Tolima_data.to_csv('C:\\Cifras_Colombia\\2021\Regionales\\Tolima_data2021-II.txt', sep="\t", encoding = "utf8")
Bogota_data.to_csv('C:\\Cifras_Colombia\\2021\Regionales\\Bogota_data.txt', sep="\t", encoding = "utf8")
Huila_data.to_csv('C:\\Cifras_Colombia\\2021\Regionales\\Huila_data.txt', sep="\t", encoding = "utf8")