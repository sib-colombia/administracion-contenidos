# -*- coding: utf-8 -*-
"""
Created on Thu Feb 14 08:27:52 2019

@author: ricardo.ortiz
"""

import pandas as pd

#Data_CO
dwc_cop = pd.read_csv('C:\\Cifras_Colombia\\2018\\dwc_co_v1.csv', encoding = "utf8", sep=",")
dt = pd.read_table('C:\\Cifras_Colombia\\Capas\\dwc_co_geo_v2.txt',encoding = "utf8",sep=',')
dt = pd.read_table('C:\\Cifras_Colombia\\Capas\\dwc_co_geo-ecosistemas.csv',encoding = "utf8",sep=',')
species = pd.read_table('C:\\Cifras_Colombia\\2018\\species.txt',encoding = "utf8",sep=',')
taxo = pd.read_table('C:\\Cifras_Colombia\\2018\\taxo.txt',encoding = "utf8",sep=',')
dt_sp=pd.merge(dt,species,on='gbifID', how= 'left')
dt_sp=dt.merge(species,on='gbifID',how='left')
dt_sp_tx=pd.merge(dt,dwc_cop,on='gbifID', how= 'left')
conCoord = dt_sp[dt_sp.decimalLat != 0]

#RUNAP

runap = dt_sp[dt_sp.id_pnn.notnull()]
runap.gbifID.nunique()
runap.species.nunique()
runap['concatPNN'] = runap['id_pnn'] +' - '+  runap['categoria'] +' '+ runap['nombre']
runap.id_pnn = runap.id_pnn.astype(str)

#Registros y especies
runap_rb = runap.groupby(['concatPNN']).gbifID.nunique().reset_index()
runap_sp = runap.groupby(['concatPNN']).species.nunique().reset_index()

#Amenaza MADS threatStatus (ts)
#RRBB
runap_ts_rb = runap.groupby(['concatPNN','threatStatus']).gbifID.nunique().reset_index()
runap_ts_rb_p=runap_ts_rb.pivot('concatPNN', 'threatStatus').fillna(0)
#species
runap_ts_sp = runap.groupby(['concatPNN','threatStatus']).species.nunique().reset_index()
runap_ts_sp_p=runap_ts_sp.pivot('concatPNN', 'threatStatus').fillna(0)

#Amenaza CITES appendixCITES (ac)
#RRBB
runap_ac_rb = runap.groupby(['concatPNN','appendixCITES']).gbifID.nunique().reset_index()
runap_ac_rb_p=runap_ac_rb.pivot('concatPNN', 'appendixCITES').fillna(0)
#species
runap_ac_sp = runap.groupby(['concatPNN','appendixCITES']).species.nunique().reset_index()
runap_ac_sp_p=runap_ac_sp.pivot('concatPNN', 'appendixCITES').fillna(0)


#Endemicas #establishmentMeans
runap_sm_rb = runap.groupby(['concatPNN','establishmentMeans']).gbifID.nunique().reset_index()
runap_sm_sp = runap.groupby(['concatPNN','establishmentMeans']).species.nunique().reset_index()

#Endemicas #migratorias
runap_m_rb = runap.groupby(['concatPNN','migratory']).gbifID.nunique().reset_index()
runap_m_sp = runap.groupby(['concatPNN','migratory']).species.nunique().reset_index()

#Endemicas #exóticas
runap_e_rb = runap.groupby(['concatPNN','exotic']).gbifID.nunique().reset_index()
runap_e_sp = runap.groupby(['concatPNN','exotic']).species.nunique().reset_index()

runap_cf = pd.merge(runap_rb,runap_sp,on='concatPNN', how= 'left')
runap_cf = pd.merge(runap_cf,runap_ts_rb_p,on='concatPNN', how= 'left')
runap_cf = pd.merge(runap_cf,runap_ts_sp_p,on='concatPNN', how= 'left')
runap_cf = pd.merge(runap_cf,runap_ac_rb_p,on='concatPNN', how= 'left')
runap_cf = pd.merge(runap_cf,runap_ac_sp_p,on='concatPNN', how= 'left')
runap_cf = pd.merge(runap_cf,runap_sm_rb,on='concatPNN', how= 'left')
runap_cf = pd.merge(runap_cf,runap_sm_sp,on='concatPNN', how= 'left')
runap_cf = pd.merge(runap_cf,runap_m_rb,on='concatPNN', how= 'left')
runap_cf = pd.merge(runap_cf,runap_m_sp,on='concatPNN', how= 'left')
runap_cf = pd.merge(runap_cf,runap_e_rb,on='concatPNN', how= 'left')
runap_cf = pd.merge(runap_cf,runap_e_sp,on='concatPNN', how= 'left')

runap_cf.to_csv('C:\\Cifras_Colombia\\2018\\Cifras2\\runap_cf_area.csv', sep=",", encoding = "utf8")



#REGIONES C&M

#Continental
regc_rb = dt_sp.groupby(['FIRST_Regi']).gbifID.nunique().reset_index()
regc_sp = dt_sp.groupby(['FIRST_Regi']).species.nunique().reset_index()

#Marítimo
regm_rb = dt_sp.groupby(['DESCRIP']).gbifID.nunique().reset_index()
regm_sp = dt_sp.groupby(['DESCRIP']).species.nunique().reset_index()


regc_c = pd.merge(regc_rb,regc_sp,on='FIRST_Regi').reset_index()
regc_c.rename(columns={'FIRST_Regi': 'DESCRIP'}, inplace=True)
regm_f = pd.merge(regm_rb,regm_sp,on='DESCRIP').reset_index()
reg_cf=pd.concat([regc_c,regm_f], sort = False)

reg_cf.to_csv('C:\\Cifras_Colombia\\2018\\Cifras\\reg_cf.csv', sep=",", encoding = "utf8")


#CORPORACIONES

car_rb = dt_sp.groupby(['NOMBRE_CAR', 'Nombre_1']).gbifID.nunique().reset_index()
car_sp = dt_sp.groupby(['NOMBRE_CAR', 'Nombre_1']).species.nunique().reset_index()

#Amenaza MADS threatStatus (ts)
#RRBB
car_ts_rb = dt_sp.groupby(['Nombre_1','threatStatus']).gbifID.nunique().reset_index()
car_ts_rb=car_ts_rb.pivot('Nombre_1','threatStatus').fillna(0)
#species
car_ts_sp = dt_sp.groupby(['Nombre_1','threatStatus']).species.nunique().reset_index()
car_ts_sp =car_ts_sp.pivot('Nombre_1', 'threatStatus').fillna(0)

#Amenaza CITES appendixCITES (ac)
#RRBB
car_ac_rb = dt_sp.groupby(['Nombre_1','appendixCITES']).gbifID.nunique().reset_index()
car_ac_rb =car_ac_rb.pivot('Nombre_1', 'appendixCITES').fillna(0)
#species
car_ac_sp = dt_sp.groupby(['Nombre_1','appendixCITES']).species.nunique().reset_index()
car_ac_sp =car_ac_sp.pivot('Nombre_1', 'appendixCITES').fillna(0)

#Endemicas #establishmentMeans
car_sm_rb = dt_sp.groupby(['Nombre_1','establishmentMeans']).gbifID.nunique().reset_index()
car_sm_sp = dt_sp.groupby(['Nombre_1','establishmentMeans']).species.nunique().reset_index()

#Endemicas #migratorias
car_m_rb = dt_sp.groupby(['Nombre_1','migratory']).gbifID.nunique().reset_index()
car_m_sp = dt_sp.groupby(['Nombre_1','migratory']).species.nunique().reset_index()

#Endemicas #exóticas
car_e_rb = dt_sp.groupby(['Nombre_1','exotic']).gbifID.nunique().reset_index()
car_e_sp = dt_sp.groupby(['Nombre_1','exotic']).species.nunique().reset_index()


car_cf = pd.merge(car_rb,car_sp,on='Nombre_1', how= 'left')
car_cf = pd.merge(car_cf,car_ts_rb,on='Nombre_1', how= 'left')
car_cf = pd.merge(car_cf,car_ts_sp,on='Nombre_1', how= 'left')
car_cf = pd.merge(car_cf,car_ac_rb,on='Nombre_1', how= 'left')
car_cf = pd.merge(car_cf,car_ac_sp,on='Nombre_1', how= 'left')
car_cf = pd.merge(car_cf,car_sm_rb,on='Nombre_1', how= 'left')
car_cf = pd.merge(car_cf,car_sm_sp,on='Nombre_1', how= 'left')
car_cf = pd.merge(car_cf,car_m_rb,on='Nombre_1', how= 'left')
car_cf = pd.merge(car_cf,car_m_sp,on='Nombre_1', how= 'left')
car_cf = pd.merge(car_cf,car_e_rb,on='Nombre_1', how= 'left')
car_cf = pd.merge(car_cf,car_e_sp,on='Nombre_1', how= 'left')

car_cf.to_csv('C:\\Cifras_Colombia\\2018\\Cifras2\\car_cf.csv', sep=",", encoding = "utf8")


#ECOSISTEMA ESTRATÉGICOS

#Páramo
#Verificación
paramo = dt_sp[dt_sp.tipo.notnull()]
paramoe = paramo[paramo.establishmentMeans.notnull()]#subset de especies endémicas en áreas de páramo
paramo.gbifID.nunique()
paramo.species.nunique()


#Amenaza MADS threatStatus (ts)
#RRBB
paramo0= dt_sp.groupby(['tipo']).gbifID.nunique().reset_index()
#species
paramo1= dt_sp.groupby(['tipo']).species.nunique().reset_index()
paramo2 = dt_sp.groupby(['tipo','threatStatus']).species.nunique().reset_index()
paramo2=paramo2.pivot('tipo', 'threatStatus').fillna(0)

paramo_total = pd.merge(paramo0,paramo1,on='tipo', how= 'left')
paramo_total = pd.merge(paramo_total,paramo2,on='tipo', how= 'left')

paramo_total.to_csv('C:\\Cifras_Colombia\\2018\\Cifras\\paramo_total.csv', sep=",", encoding = "utf8")


#Amenaza MADS threatStatus (ts) para especies endémicas en áreas de páramo

paramo0= paramoe.groupby(['tipo']).gbifID.nunique().reset_index()
#species
paramo1= paramoe.groupby(['tipo']).species.nunique().reset_index()
paramo2 = paramoe.groupby(['tipo','threatStatus']).species.nunique().reset_index()
paramo2=paramo2.pivot('tipo', 'threatStatus').fillna(0)

paramo_total = pd.merge(paramo0,paramo1,on='tipo', how= 'left')
paramo_total_e = pd.merge(paramo_total,paramo2,on='tipo', how= 'left')

paramo_total_e.to_csv('C:\\Cifras_Colombia\\2018\\Cifras\\paramo_total_e.csv', sep=",", encoding = "utf8")



#Registros y especies
paramo_rb = dt_sp.groupby(['cmpljnom']).gbifID.nunique().reset_index()
paramo_sp = dt_sp.groupby(['cmpljnom']).species.nunique().reset_index()

#Amenaza MADS threatStatus (ts)
#RRBB
paramo_ts_rb = dt_sp.groupby(['cmpljnom','threatStatus']).gbifID.nunique().reset_index()
paramo_ts_rb=paramo_ts_rb.pivot('cmpljnom', 'threatStatus').fillna(0)
#species
paramo_ts_sp = dt_sp.groupby(['cmpljnom','threatStatus']).species.nunique().reset_index()
paramo_ts_sp=paramo_ts_sp.pivot('cmpljnom', 'threatStatus').fillna(0)

#Amenaza CITES appendixCITES (ac)
#RRBB
paramo_ac_rb = dt_sp.groupby(['cmpljnom','appendixCITES']).gbifID.nunique().reset_index()
paramo_ac_rb=paramo_ac_rb.pivot('cmpljnom', 'appendixCITES').fillna(0)
#species
paramo_ac_sp = dt_sp.groupby(['cmpljnom','appendixCITES']).species.nunique().reset_index()
paramo_ac_sp=paramo_ac_sp.pivot('cmpljnom', 'appendixCITES').fillna(0)


#Endemicas #establishmentMeans
paramo_sm_rb = dt_sp.groupby(['cmpljnom','establishmentMeans']).gbifID.nunique().reset_index()
paramo_sm_sp = dt_sp.groupby(['cmpljnom','establishmentMeans']).species.nunique().reset_index()

#Endemicas #migratorias
paramo_m_rb = dt_sp.groupby(['cmpljnom','migratory']).gbifID.nunique().reset_index()
paramo_m_sp = dt_sp.groupby(['cmpljnom','migratory']).species.nunique().reset_index()

#Endemicas #exóticas
paramo_e_rb = dt_sp.groupby(['cmpljnom','exotic']).gbifID.nunique().reset_index()
paramo_e_sp = dt_sp.groupby(['cmpljnom','exotic']).species.nunique().reset_index()



paramo_cf = pd.merge(paramo_rb,paramo_sp,on='cmpljnom', how= 'left')
paramo_cf = pd.merge(paramo_cf,paramo_ts_rb,on='cmpljnom', how= 'left')
paramo_cf = pd.merge(paramo_cf,paramo_ts_sp,on='cmpljnom', how= 'left')
paramo_cf = pd.merge(paramo_cf,paramo_ac_rb,on='cmpljnom', how= 'left')
paramo_cf = pd.merge(paramo_cf,paramo_ac_sp,on='cmpljnom', how= 'left')
paramo_cf = pd.merge(paramo_cf,paramo_sm_rb,on='cmpljnom', how= 'left')
paramo_cf = pd.merge(paramo_cf,paramo_sm_sp,on='cmpljnom', how= 'left')
paramo_cf = pd.merge(paramo_cf,paramo_m_rb,on='cmpljnom', how= 'left')
paramo_cf = pd.merge(paramo_cf,paramo_m_sp,on='cmpljnom', how= 'left')
paramo_cf = pd.merge(paramo_cf,paramo_e_rb,on='cmpljnom', how= 'left')
paramo_cf = pd.merge(paramo_cf,paramo_e_sp,on='cmpljnom', how= 'left')

paramo_cf.to_csv('C:\\Cifras_Colombia\\2018\\Cifras\\paramo_cf.csv', sep=",", encoding = "utf8")




#BST
#Verificación
bst = dt_sp[dt_sp.COBERTURA.notnull()]
bste = bst[dt_sp.establishmentMeans.notnull()]
bst.gbifID.nunique()
bst.species.nunique()

#Amenaza MADS threatStatus (ts)
#RRBB
bst0= dt_sp.groupby(['COBERTURA']).gbifID.nunique().reset_index()
#species
bst1= dt_sp.groupby(['COBERTURA']).species.nunique().reset_index()
bst2 = dt_sp.groupby(['COBERTURA','threatStatus']).species.nunique().reset_index()
bst2=bst2.pivot('COBERTURA', 'threatStatus').fillna(0)

bst_total = pd.merge(bst0,bst1,on='COBERTURA', how= 'left')
bst_total = pd.merge(bst_total,bst2,on='COBERTURA', how= 'left')

bst_total.to_csv('C:\\Cifras_Colombia\\2018\\Cifras\\bst_total.csv', sep=",", encoding = "utf8")

#Endémicas
#Amenaza MADS threatStatus (ts)
#RRBB
bst0e= bste.groupby(['COBERTURA']).gbifID.nunique().reset_index()
#species
bst1e= bste.groupby(['COBERTURA']).species.nunique().reset_index()
bst2e = bste.groupby(['COBERTURA','threatStatus']).species.nunique().reset_index()
bst2e=bst2e.pivot('COBERTURA', 'threatStatus').fillna(0)

bst_totale = pd.merge(bst0e,bst1e,on='COBERTURA', how= 'left')
bst_totale = pd.merge(bst_totale,bst2e,on='COBERTURA', how= 'left')

bst_totale.to_csv('C:\\Cifras_Colombia\\2018\\Cifras\\bst_totale.csv', sep=",", encoding = "utf8")





#AICA's
#Verificación

#RRBB
aicas = dt_sp_tx[dt_sp_tx.AICA.notnull()]
aicas.rename(columns={'class': 'clase'}, inplace=True)
aicas=aicas[aicas.clase == 'Aves']
aicas.gbifID.nunique()
aicas.species.nunique()

#Amenaza MADS threatStatus (ts)
#RRBB
aicas1= aicas.groupby(['threatStatus']).gbifID.nunique().reset_index()
#species
aicas2 = aicas.groupby(['threatStatus']).species.nunique().reset_index()
aicas_total = pd.merge(aicas1,aicas2,on='threatStatus', how= 'left')


#HUMEDALES
#Verificación

#RRBB
hum = dt_sp[dt_sp.HUMEDALES.notnull()]
hum = hum.loc[hum['HUMEDALES'] != 'No data']
hume = hume[hume.establishmentMeans.notnull()]

hum.gbifID.nunique()
hum.species.nunique()

#Amenaza MADS threatStatus (ts)
#RRBB
hum1= hum.groupby(['threatStatus']).gbifID.nunique().reset_index()
#species
hum2 = hum.groupby(['threatStatus']).species.nunique().reset_index()
hum_total = pd.merge(hum1,hum2,on='threatStatus', how= 'left')




#RESGUARDO INDÍGENAS

#Resguardos

resguardos = dt_sp[dt_sp.TIPO_1.notnull()]
resguardos.to_csv('C:\\Cifras_Colombia\\2018\\Cifras\\resguardos_prueba.csv', sep=",", encoding = "utf8")
resguardos.gbifID.nunique()
resguardos.species.nunique()



#Registros y especies
resguardos_rb = dt_sp.groupby(['TIPO_1']).gbifID.nunique().reset_index()
resguardos_sp = dt_sp.groupby(['TIPO_1']).species.nunique().reset_index()

#Amenaza MADS threatStatus (ts)
#RRBB
resguardos_ts_rb = dt_sp.groupby(['TIPO_1','threatStatus']).gbifID.nunique().reset_index()
resguardos_ts_rb_p=resguardos_ts_rb.pivot('TIPO_1', 'threatStatus').fillna(0)
#species
resguardos_ts_sp = dt_sp.groupby(['TIPO_1','threatStatus']).species.nunique().reset_index()
resguardos_ts_sp_p=resguardos_ts_sp.pivot('TIPO_1', 'threatStatus').fillna(0)

#Amenaza CITES appendixCITES (ac)
#RRBB
resguardos_ac_rb = dt_sp.groupby(['TIPO_1','appendixCITES']).gbifID.nunique().reset_index()
resguardos_ac_rb_p=resguardos_ac_rb.pivot('TIPO_1', 'appendixCITES').fillna(0)
#species
resguardos_ac_sp = dt_sp.groupby(['TIPO_1','appendixCITES']).species.nunique().reset_index()
resguardos_ac_sp_p=resguardos_ac_sp.pivot('TIPO_1', 'appendixCITES').fillna(0)


#Endemicas #establishmentMeans
resguardos_sm_rb = dt_sp.groupby(['TIPO_1','establishmentMeans']).gbifID.nunique().reset_index()
resguardos_sm_sp = dt_sp.groupby(['TIPO_1','establishmentMeans']).species.nunique().reset_index()

#Endemicas #migratorias
resguardos_m_rb = dt_sp.groupby(['TIPO_1','migratory']).gbifID.nunique().reset_index()
resguardos_m_sp = dt_sp.groupby(['TIPO_1','migratory']).species.nunique().reset_index()

#Endemicas #exóticas
resguardos_e_rb = dt_sp.groupby(['TIPO_1','exotic']).gbifID.nunique().reset_index()
resguardos_e_sp = dt_sp.groupby(['TIPO_1','exotic']).species.nunique().reset_index()

resguardos_cf = pd.merge(resguardos_rb,resguardos_sp,on='TIPO_1', how= 'left')
resguardos_cf = pd.merge(resguardos_cf,resguardos_ts_rb_p,on='TIPO_1', how= 'left')
resguardos_cf = pd.merge(resguardos_cf,resguardos_ts_sp_p,on='TIPO_1', how= 'left')
resguardos_cf = pd.merge(resguardos_cf,resguardos_ac_rb_p,on='TIPO_1', how= 'left')
resguardos_cf = pd.merge(resguardos_cf,resguardos_ac_sp_p,on='TIPO_1', how= 'left')
resguardos_cf = pd.merge(resguardos_cf,resguardos_sm_rb,on='TIPO_1', how= 'left')
resguardos_cf = pd.merge(resguardos_cf,resguardos_sm_sp,on='TIPO_1', how= 'left')
resguardos_cf = pd.merge(resguardos_cf,resguardos_m_rb,on='TIPO_1', how= 'left')
resguardos_cf = pd.merge(resguardos_cf,resguardos_m_sp,on='TIPO_1', how= 'left')
resguardos_cf = pd.merge(resguardos_cf,resguardos_e_rb,on='TIPO_1', how= 'left')
resguardos_cf = pd.merge(resguardos_cf,resguardos_e_sp,on='TIPO_1', how= 'left')

resguardos_cf.to_csv('C:\\Cifras_Colombia\\2018\\Cifras\\resguardos_Total.csv', sep=",", encoding = "utf8")


#Resguardos X Etnia

resguardos = dt_sp[dt_sp.categoria.notnull()]
resguardos.gbifID.nunique()
resguardos.species.nunique()



#Registros y especies
resguardos_rb = dt_sp.groupby(['Etnia']).gbifID.nunique().reset_index()
resguardos_sp = dt_sp.groupby(['Etnia']).species.nunique().reset_index()

#Amenaza MADS threatStatus (ts)
#RRBB
resguardos_ts_rb = dt_sp.groupby(['Etnia','threatStatus']).gbifID.nunique().reset_index()
resguardos_ts_rb_p=resguardos_ts_rb.pivot('Etnia', 'threatStatus').fillna(0)
#species
resguardos_ts_sp = dt_sp.groupby(['Etnia','threatStatus']).species.nunique().reset_index()
resguardos_ts_sp_p=resguardos_ts_sp.pivot('Etnia', 'threatStatus').fillna(0)

#Amenaza CITES appendixCITES (ac)
#RRBB
resguardos_ac_rb = dt_sp.groupby(['Etnia','appendixCITES']).gbifID.nunique().reset_index()
resguardos_ac_rb_p=resguardos_ac_rb.pivot('Etnia', 'appendixCITES').fillna(0)
#species
resguardos_ac_sp = dt_sp.groupby(['Etnia','appendixCITES']).species.nunique().reset_index()
resguardos_ac_sp_p=resguardos_ac_sp.pivot('Etnia', 'appendixCITES').fillna(0)


#Endemicas #establishmentMeans
resguardos_sm_rb = dt_sp.groupby(['Etnia','establishmentMeans']).gbifID.nunique().reset_index()
resguardos_sm_sp = dt_sp.groupby(['Etnia','establishmentMeans']).species.nunique().reset_index()

#Endemicas #migratorias
resguardos_m_rb = dt_sp.groupby(['Etnia','migratory']).gbifID.nunique().reset_index()
resguardos_m_sp = dt_sp.groupby(['Etnia','migratory']).species.nunique().reset_index()

#Endemicas #exóticas
resguardos_e_rb = dt_sp.groupby(['Etnia','exotic']).gbifID.nunique().reset_index()
resguardos_e_sp = dt_sp.groupby(['Etnia','exotic']).species.nunique().reset_index()

resguardos_cf = pd.merge(resguardos_rb,resguardos_sp,on='Etnia', how= 'left')
resguardos_cf = pd.merge(resguardos_cf,resguardos_ts_rb_p,on='Etnia', how= 'left')
resguardos_cf = pd.merge(resguardos_cf,resguardos_ts_sp_p,on='Etnia', how= 'left')
resguardos_cf = pd.merge(resguardos_cf,resguardos_ac_rb_p,on='Etnia', how= 'left')
resguardos_cf = pd.merge(resguardos_cf,resguardos_ac_sp_p,on='Etnia', how= 'left')
resguardos_cf = pd.merge(resguardos_cf,resguardos_sm_rb,on='Etnia', how= 'left')
resguardos_cf = pd.merge(resguardos_cf,resguardos_sm_sp,on='Etnia', how= 'left')
resguardos_cf = pd.merge(resguardos_cf,resguardos_m_rb,on='Etnia', how= 'left')
resguardos_cf = pd.merge(resguardos_cf,resguardos_m_sp,on='Etnia', how= 'left')
resguardos_cf = pd.merge(resguardos_cf,resguardos_e_rb,on='Etnia', how= 'left')
resguardos_cf = pd.merge(resguardos_cf,resguardos_e_sp,on='Etnia', how= 'left')

resguardos_cf.to_csv('C:\\Cifras_Colombia\\2018\\Cifras\\resguardos_Etnia.csv', sep=",", encoding = "utf8")

#Resguardos X Nombre de resguardo

resguardos = dt_sp[dt_sp.categoria.notnull()]
resguardos.gbifID.nunique()
resguardos.species.nunique()



#Registros y especies
resguardos_rb = dt_sp.groupby(['NomDpto']).gbifID.nunique().reset_index()
resguardos_sp = dt_sp.groupby(['NomDpto']).species.nunique().reset_index()

#Amenaza MADS threatStatus (ts)
#RRBB
resguardos_ts_rb = dt_sp.groupby(['NomDpto','threatStatus']).gbifID.nunique().reset_index()
resguardos_ts_rb_p=resguardos_ts_rb.pivot('NomDpto', 'threatStatus').fillna(0)
#species
resguardos_ts_sp = dt_sp.groupby(['NomDpto','threatStatus']).species.nunique().reset_index()
resguardos_ts_sp_p=resguardos_ts_sp.pivot('NomDpto', 'threatStatus').fillna(0)

#Amenaza CITES appendixCITES (ac)
#RRBB
resguardos_ac_rb = dt_sp.groupby(['NomDpto','appendixCITES']).gbifID.nunique().reset_index()
resguardos_ac_rb_p=resguardos_ac_rb.pivot('NomDpto', 'appendixCITES').fillna(0)
#species
resguardos_ac_sp = dt_sp.groupby(['NomDpto','appendixCITES']).species.nunique().reset_index()
resguardos_ac_sp_p=resguardos_ac_sp.pivot('NomDpto', 'appendixCITES').fillna(0)


#Endemicas #establishmentMeans
resguardos_sm_rb = dt_sp.groupby(['NomDpto','establishmentMeans']).gbifID.nunique().reset_index()
resguardos_sm_sp = dt_sp.groupby(['NomDpto','establishmentMeans']).species.nunique().reset_index()

#Endemicas #migratorias
resguardos_m_rb = dt_sp.groupby(['NomDpto','migratory']).gbifID.nunique().reset_index()
resguardos_m_sp = dt_sp.groupby(['NomDpto','migratory']).species.nunique().reset_index()

#Endemicas #exóticas
resguardos_e_rb = dt_sp.groupby(['NomDpto','exotic']).gbifID.nunique().reset_index()
resguardos_e_sp = dt_sp.groupby(['NomDpto','exotic']).species.nunique().reset_index()

resguardos_cf = pd.merge(resguardos_rb,resguardos_sp,on='NomDpto', how= 'left')
resguardos_cf = pd.merge(resguardos_cf,resguardos_ts_rb_p,on='NomDpto', how= 'left')
resguardos_cf = pd.merge(resguardos_cf,resguardos_ts_sp_p,on='NomDpto', how= 'left')
resguardos_cf = pd.merge(resguardos_cf,resguardos_ac_rb_p,on='NomDpto', how= 'left')
resguardos_cf = pd.merge(resguardos_cf,resguardos_ac_sp_p,on='NomDpto', how= 'left')
resguardos_cf = pd.merge(resguardos_cf,resguardos_sm_rb,on='NomDpto', how= 'left')
resguardos_cf = pd.merge(resguardos_cf,resguardos_sm_sp,on='NomDpto', how= 'left')
resguardos_cf = pd.merge(resguardos_cf,resguardos_m_rb,on='NomDpto', how= 'left')
resguardos_cf = pd.merge(resguardos_cf,resguardos_m_sp,on='NomDpto', how= 'left')
resguardos_cf = pd.merge(resguardos_cf,resguardos_e_rb,on='NomDpto', how= 'left')
resguardos_cf = pd.merge(resguardos_cf,resguardos_e_sp,on='NomDpto', how= 'left')

resguardos_cf.to_csv('C:\\Cifras_Colombia\\2018\\Cifras\\resguardos_NDepto.csv', sep=",", encoding = "utf8")

#MUNICIPIOS 1:25.000 2014

mpios = dwc_cop
mpios.COD_DANE = mpios.COD_DANE.astype(str)
mpios['concatDANE'] = mpios['COD_DANE'] +' - '+  mpios['Departamen'] +' - '+ mpios['Municipio']
#mpios['establishmentMeans']=mpios['establishmentMeans'].str.replace('Endémicas', 'Endémica')

#Registros y especies
mpios_rb = mpios.groupby(['concatDANE']).gbifID.nunique().reset_index()
mpios_sp = mpios.groupby(['concatDANE']).species.nunique().reset_index()

#Amenaza MADS threatStatus (ts)
#RRBB
mpios_ts_rb = mpios.groupby(['concatDANE','threatStatus']).gbifID.nunique().reset_index()
mpios_ts_rb_p=mpios_ts_rb.pivot('concatDANE', 'threatStatus').fillna(0)
#species
mpios_ts_sp = mpios.groupby(['concatDANE','threatStatus']).species.nunique().reset_index()
mpios_ts_sp_p=mpios_ts_sp.pivot('concatDANE', 'threatStatus').fillna(0)

#Amenaza CITES appendixCITES (ac)
#RRBB
mpios_ac_rb = mpios.groupby(['concatDANE','appendixCITES']).gbifID.nunique().reset_index()
mpios_ac_rb_p=mpios_ac_rb.pivot('concatDANE', 'appendixCITES').fillna(0)
#species
mpios_ac_sp = mpios.groupby(['concatDANE','appendixCITES']).species.nunique().reset_index()
mpios_ac_sp_p=mpios_ac_sp.pivot('concatDANE', 'appendixCITES').fillna(0)


#Endemicas #establishmentMeans
mpios_sm_rb = mpios.groupby(['concatDANE','establishmentMeans']).gbifID.nunique().reset_index()
mpios_sm_sp = mpios.groupby(['concatDANE','establishmentMeans']).species.nunique().reset_index()

#Endemicas #migratorias
mpios_m_rb = mpios.groupby(['concatDANE','migratory']).gbifID.nunique().reset_index()
mpios_m_sp = mpios.groupby(['concatDANE','migratory']).species.nunique().reset_index()

#Endemicas #exóticas
mpios_e_rb = mpios.groupby(['concatDANE','exotic']).gbifID.nunique().reset_index()
mpios_e_sp = mpios.groupby(['concatDANE','exotic']).species.nunique().reset_index()

mpios_cf = pd.merge(mpios_rb,mpios_sp,on='concatDANE', how= 'left')
mpios_cf = pd.merge(mpios_cf,mpios_ts_rb_p,on='concatDANE', how= 'left')
mpios_cf = pd.merge(mpios_cf,mpios_ts_sp_p,on='concatDANE', how= 'left')
mpios_cf = pd.merge(mpios_cf,mpios_ac_rb_p,on='concatDANE', how= 'left')
mpios_cf = pd.merge(mpios_cf,mpios_ac_sp_p,on='concatDANE', how= 'left')
mpios_cf = pd.merge(mpios_cf,mpios_sm_rb,on='concatDANE', how= 'left')
mpios_cf = pd.merge(mpios_cf,mpios_sm_sp,on='concatDANE', how= 'left')
mpios_cf = pd.merge(mpios_cf,mpios_m_rb,on='concatDANE', how= 'left')
mpios_cf = pd.merge(mpios_cf,mpios_m_sp,on='concatDANE', how= 'left')
mpios_cf = pd.merge(mpios_cf,mpios_e_rb,on='concatDANE', how= 'left')
mpios_cf = pd.merge(mpios_cf,mpios_e_sp,on='concatDANE', how= 'left')

mpios_cf.to_csv('C:\\Cifras_Colombia\\2018\\Cifras2\\mpios_cf.csv', sep=",", encoding = "utf8")


#ECOSISTEMAS ideam 2018

ecosistemas=dt_sp

#Registros y especies
ecosistemas_rb = ecosistemas.groupby(['ECOS_SINTE']).gbifID.nunique().reset_index()
ecosistemas_sp = ecosistemas.groupby(['ECOS_SINTE']).species.nunique().reset_index()

#Amenaza MADS threatStatus (ts)
#RRBB
ecosistemas_ts_rb = ecosistemas.groupby(['ECOS_SINTE','threatStatus']).gbifID.nunique().reset_index()
ecosistemas_ts_rb_p=ecosistemas_ts_rb.pivot('ECOS_SINTE', 'threatStatus').fillna(0)
#species
ecosistemas_ts_sp = ecosistemas.groupby(['ECOS_SINTE','threatStatus']).species.nunique().reset_index()
ecosistemas_ts_sp_p=ecosistemas_ts_sp.pivot('ECOS_SINTE', 'threatStatus').fillna(0)

#Amenaza CITES appendixCITES (ac)
#RRBB
ecosistemas_ac_rb = ecosistemas.groupby(['ECOS_SINTE','appendixCITES']).gbifID.nunique().reset_index()
ecosistemas_ac_rb_p=ecosistemas_ac_rb.pivot('ECOS_SINTE', 'appendixCITES').fillna(0)
#species
ecosistemas_ac_sp = ecosistemas.groupby(['ECOS_SINTE','appendixCITES']).species.nunique().reset_index()
ecosistemas_ac_sp_p=ecosistemas_ac_sp.pivot('ECOS_SINTE', 'appendixCITES').fillna(0)


#Endemicas #establishmentMeans
ecosistemas_sm_rb = ecosistemas.groupby(['ECOS_SINTE','establishmentMeans']).gbifID.nunique().reset_index()
ecosistemas_sm_sp = ecosistemas.groupby(['ECOS_SINTE','establishmentMeans']).species.nunique().reset_index()

#Endemicas #migratorias
ecosistemas_m_rb = ecosistemas.groupby(['ECOS_SINTE','migratory']).gbifID.nunique().reset_index()
ecosistemas_m_sp = ecosistemas.groupby(['ECOS_SINTE','migratory']).species.nunique().reset_index()

#Endemicas #exóticas
ecosistemas_e_rb = ecosistemas.groupby(['ECOS_SINTE','exotic']).gbifID.nunique().reset_index()
ecosistemas_e_sp = ecosistemas.groupby(['ECOS_SINTE','exotic']).species.nunique().reset_index()

ecosistemas_cf = pd.merge(ecosistemas_rb,ecosistemas_sp,on='ECOS_SINTE', how= 'left')
ecosistemas_cf = pd.merge(ecosistemas_cf,ecosistemas_ts_rb_p,on='ECOS_SINTE', how= 'left')
ecosistemas_cf = pd.merge(ecosistemas_cf,ecosistemas_ts_sp_p,on='ECOS_SINTE', how= 'left')
ecosistemas_cf = pd.merge(ecosistemas_cf,ecosistemas_ac_rb_p,on='ECOS_SINTE', how= 'left')
ecosistemas_cf = pd.merge(ecosistemas_cf,ecosistemas_ac_sp_p,on='ECOS_SINTE', how= 'left')
ecosistemas_cf = pd.merge(ecosistemas_cf,ecosistemas_sm_rb,on='ECOS_SINTE', how= 'left')
ecosistemas_cf = pd.merge(ecosistemas_cf,ecosistemas_sm_sp,on='ECOS_SINTE', how= 'left')
ecosistemas_cf = pd.merge(ecosistemas_cf,ecosistemas_m_rb,on='ECOS_SINTE', how= 'left')
ecosistemas_cf = pd.merge(ecosistemas_cf,ecosistemas_m_sp,on='ECOS_SINTE', how= 'left')
ecosistemas_cf = pd.merge(ecosistemas_cf,ecosistemas_e_rb,on='ECOS_SINTE', how= 'left')
ecosistemas_cf = pd.merge(ecosistemas_cf,ecosistemas_e_sp,on='ECOS_SINTE', how= 'left')

ecosistemas_cf.to_csv('C:\\Cifras_Colombia\\2018\\Cifras2\\ecosistemas_sint_cf.csv', sep=",", encoding = "utf8")



