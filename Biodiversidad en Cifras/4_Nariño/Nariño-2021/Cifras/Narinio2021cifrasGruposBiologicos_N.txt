	grupoBio	registros	especies	especiesAmenaza	especiesEN	especiesCR	especiesVU	registrosAmenaza	registrosCR	registrosEN	registrosVU	especiesCites	especiesCitesI	especiesCitesII	especiesCitesIII	registrosCites	registrosCitesI	registrosCitesII	registrosCitesIII	especiesEndemicas	especiesExoticas	especiesMigratorias	registrosEndemicas	registrosExoticas	registrosMigratorias	especiesInvasora	registrosInvasora	especiesInvasora GRIIS	registrosInvasora GRIIS	especiesthreatStatus_UICN	especiesEW_UICN	especiesCR_UICN	especiesEN_UICN	especiesVU_UICN	especiesNT_UICN	especiesLC_UICN	registrosthreatStatus_UICN	registrosEW_UICN	registrosCR_UICN	registrosEN_UICN	registrosVU_UICN	registrosNT_UICN	registrosLC_UICN	registrosDD_UICN	registrosisInvasive_griis	registrosisInvasive_mads	registrosMigratorio	especiesisInvasive_griis	especiesisInvasive_mads	especiesMigratorio	registrosEndemicas2	registrosExoticas2	registrosLR/lc_UICN	registrosLR/nt_UICN	especiesEndemicas2	especiesExoticas2	especiesLR/lc_UICN	especiesLR/nt_UICN
0	Abejas	59	9	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4	0	0	0	0	0	1	29	0	0	0	0	0	1	28	0	0	0	0	0	0	0	0	0	0	0	0	0	0
1	Algas	2803	9	0	nan	nan	nan	0	nan	nan	nan	0	nan	nan	nan	0	nan	nan	nan	0	0	nan	0	0	nan	nan	nan	nan	nan	0	nan	nan	nan	nan	nan	nan	0	nan	nan	nan	nan	nan	nan	nan	0	0	0	0	0	0	nan	nan	nan	nan	nan	nan	nan	nan
2	Anfibios	6432	110	4	3	1	0	286	271	15	0	4	0	4	0	1567	0	1567	0	0	0	0	0	0	0	0	0	0	0	94	0	6	11	15	10	48	5233	0	1711	127	452	170	2069	704	0	0	0	0	0	0	0	0	0	0	0	0	0	0
3	Animales	155040	2529	71	28	5	38	3084	361	1012	1711	211	12	191	8	20432	132	19348	952	36	11	104	264	959	12223	1	3	7	942	1523	0	8	24	58	69	1326	134096	0	1717	224	1893	3033	126412	813	942	3	12223	7	1	104	264	959	0	4	36	11	0	2
4	Arácnidos	671	85	0	nan	nan	nan	0	nan	nan	nan	0	nan	nan	nan	0	nan	nan	nan	0	0	nan	0	0	nan	nan	nan	nan	nan	0	nan	nan	nan	nan	nan	nan	0	nan	nan	nan	nan	nan	nan	nan	0	0	0	0	0	0	nan	nan	nan	nan	nan	nan	nan	nan
5	Aves	138405	1139	45	19	4	22	2573	90	974	1509	182	3	173	6	18780	77	17756	947	9	3	104	115	924	12223	0	0	3	924	1010	0	0	6	26	44	932	126585	0	0	70	1344	2822	122323	26	924	0	12223	3	0	104	115	924	0	0	9	3	0	0
6	Bromelias, labiadas y pasifloras	2158	242	34	12	4	18	231	11	45	175	0	0	0	0	0	0	0	0	36	3	0	315	8	0	0	0	0	0	37	0	0	1	1	1	33	674	0	0	1	181	3	485	4	0	0	0	0	0	0	315	8	0	0	36	3	0	0
7	Cactus	787	6	0	0	0	0	0	0	0	0	2	0	2	0	2	0	2	0	2	0	0	2	0	0	0	0	0	0	3	0	0	1	0	0	1	3	0	0	1	0	0	1	1	0	0	0	0	0	0	2	0	0	0	2	0	0	0
8	Dipteros	810	42	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1	0	0	1	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1	0	0	0	1	0	0
9	Escarabajos	1473	90	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	5	0	0	0	0	0	5	62	0	0	0	0	0	62	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0
10	Especies Maderables	88	7	7	3	2	2	88	5	8	75	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	6	0	0	1	3	0	2	87	0	0	3	9	0	75	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0
11	Fanerógamas	5810	24	6	3	0	3	10	0	3	7	0	0	0	0	0	0	0	0	3	0	0	11	0	0	0	0	0	0	14	0	0	1	3	0	8	2953	0	0	4	3	0	2943	0	0	0	0	0	0	0	11	0	3	0	3	0	2	0
12	Frailejones	2136	3	1	0	0	1	108	0	0	108	0	0	0	0	0	0	0	0	2	0	0	115	0	0	0	0	0	0	3	0	0	0	1	0	2	2113	0	0	0	108	0	2005	0	0	0	0	0	0	0	115	0	0	0	2	0	0	0
13	Helechos y afines	27513	578	0	0	0	0	0	0	0	0	31	0	31	0	22250	0	22250	0	9	1	0	21	12	0	0	0	0	0	6	0	0	0	0	0	5	23	0	0	0	0	0	20	3	0	0	0	0	0	0	21	12	0	0	9	1	0	0
14	Hongos	2161	516	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1	0	0	4	0	0	0	0	0	0	3	0	0	0	2	1	0	7	0	0	0	5	2	0	0	0	0	0	0	0	0	4	0	0	0	1	0	0	0
15	Hormigas	393	54	0	nan	nan	nan	0	nan	nan	nan	0	nan	nan	nan	0	nan	nan	nan	0	0	nan	0	0	nan	nan	nan	nan	nan	0	nan	nan	nan	nan	nan	nan	0	nan	nan	nan	nan	nan	nan	nan	0	0	0	0	0	0	nan	nan	nan	nan	nan	nan	nan	nan
16	Insectos	4110	429	1	0	0	1	1	0	0	1	0	0	0	0	0	0	0	0	0	1	0	0	1	0	0	0	0	0	11	0	0	0	0	0	8	93	0	0	0	0	0	65	28	0	0	0	0	0	0	0	1	0	0	0	1	0	0
17	Invertebrados	6620	802	3	0	0	3	141	0	0	141	0	0	0	0	0	0	0	0	0	3	0	0	5	0	0	0	0	0	25	0	0	0	1	0	18	113	0	0	0	1	0	81	31	0	0	0	0	0	0	0	5	0	0	0	3	0	0
18	Líquenes	1768	394	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1	0	0	4	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	4	0	0	0	1	0	0	0
19	Magnolias y afines	4820	30	8	2	1	5	4702	11	21	4670	0	0	0	0	0	0	0	0	4	0	0	30	0	0	0	0	0	0	18	0	1	2	0	0	15	77	0	11	21	0	0	45	0	0	0	0	0	0	0	30	0	0	0	4	0	0	0
20	Mamíferos	1553	133	8	3	0	5	15	0	3	12	19	7	10	2	39	19	15	5	4	3	0	51	15	0	0	0	3	15	114	0	0	2	8	8	87	1142	0	0	2	22	8	1091	19	15	0	0	3	0	0	51	15	0	0	4	3	0	0
21	Mariposas	321	142	1	0	0	1	1	0	0	1	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2	0	0	0	0	0	2	2	0	0	0	0	0	2	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0
22	Moluscos	1000	174	2	0	0	2	140	0	0	140	0	0	0	0	0	0	0	0	0	2	0	0	4	0	0	0	0	0	2	0	0	0	0	0	2	6	0	0	0	0	0	6	0	0	0	0	0	0	0	0	4	0	0	0	2	0	0
23	Musgos y afines	5214	543	4	1	1	2	13	2	1	10	0	0	0	0	0	0	0	0	4	0	0	10	0	0	0	0	0	0	1	0	0	0	0	0	1	1	0	0	0	0	0	1	0	0	0	0	0	0	0	10	0	0	0	4	0	0	0
24	Orquídeas	1521	335	5	0	0	5	9	0	0	9	169	0	169	0	515	0	515	0	49	0	0	74	0	0	0	0	0	0	9	0	0	2	0	0	7	26	0	0	2	0	0	24	0	0	0	0	0	0	0	74	0	0	0	49	0	0	0
25	Palmas	37312	55	6	5	0	1	58	0	53	5	0	0	0	0	0	0	0	0	2	0	0	31	0	0	0	0	0	0	8	0	0	1	1	1	3	2916	0	0	14	3	10	2642	0	0	0	0	0	0	0	31	0	238	9	2	0	1	1
26	Peces	1097	256	5	0	0	5	11	0	0	11	0	0	0	0	0	0	0	0	23	1	0	98	3	0	1	3	1	3	214	0	2	1	6	3	187	666	0	6	4	37	15	571	33	3	3	0	1	1	0	98	3	0	0	23	1	0	0
27	Pinos y afines	17	6	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2	0	0	8	0	0	0	2	8	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	8	0	0	2	0	0	0	8	0	0	0	2	0	0
28	Plantas	432511	6322	74	28	8	38	5210	29	135	5046	205	0	205	0	22782	0	22782	0	528	118	0	14153	833	0	3	7	16	81	1032	5	6	28	34	17	923	152668	236	39	940	2773	89	147804	92	81	7	0	16	3	0	14153	833	681	14	528	118	8	3
29	Reptiles	481	88	6	3	0	3	58	0	20	38	6	2	4	0	46	36	10	0	0	1	0	0	12	0	0	0	0	0	66	0	0	4	2	4	54	357	0	0	21	37	18	277	0	0	0	0	0	0	0	0	12	0	4	0	1	0	2
30	Vertebrados	147993	1726	68	28	5	35	2943	361	1012	1570	211	12	191	8	20432	132	19348	952	36	8	104	264	954	12223	1	3	7	942	1498	0	8	24	57	69	1308	133983	0	1717	224	1892	3033	126331	782	942	3	12223	7	1	104	264	954	0	4	36	8	0	2
31	Zamias	4	1	1	0	0	1	2	0	0	2	1	0	1	0	2	0	2	0	0	0	0	0	0	0	0	0	0	0	1	0	0	0	0	1	0	2	0	0	0	0	2	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0
