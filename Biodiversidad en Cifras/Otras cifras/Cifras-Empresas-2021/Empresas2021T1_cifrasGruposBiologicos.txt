	grupoBio	especies	registros	especiesAmenaza	especiesEN	especiesCR	especiesVU	registrosAmenaza	registrosCR	registrosEN	registrosVU	especiesCites	especiesCitesI	especiesCitesI/II	especiesCitesII	especiesCitesIII	registrosCites	registrosCitesI	registrosCitesI/II	registrosCitesII	registrosCitesIII	especiesEndemicas	especiesExoticas	especiesMigratorias	registrosEndemicas	registrosExoticas	registrosMigratorias	especiesInvasora	registrosInvasora	especiesInvasora GRIIS	registrosInvasora GRIIS	especiesthreatStatus_UICN	especiesEW_UICN	especiesCR_UICN	especiesEN_UICN	especiesVU_UICN	especiesNT_UICN	especiesLC_UICN	especiesLR/cd_UICN	especiesDD_UICN	registrosthreatStatus_UICN	registrosEW_UICN	registrosCR_UICN	registrosEN_UICN	registrosVU_UICN	registrosNT_UICN	registrosLC_UICN	registrosLR/cd_UICN	registrosDD_UICN
0	Abejas	14	333	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1	0	0	31	0	0	0	0	0	4	0	0	0	0	0	1	0	3	19	0	0	0	0	0	1	0	18
1	Algas	10	872	0	nan	nan	nan	0	nan	nan	nan	0	nan	nan	nan	nan	0	nan	nan	nan	nan	0	0	nan	0	0	nan	nan	nan	nan	nan	0	nan	nan	nan	nan	nan	nan	nan	nan	0	nan	nan	nan	nan	nan	nan	nan	nan
2	Anfibios	71	3396	2	0	0	2	22	0	0	22	1	0	0	1	0	149	0	0	149	0	0	1	0	0	3	0	1	3	1	3	66	0	0	1	5	5	55	0	0	2914	0	0	83	30	50	2751	0	0
3	Angiospermas	1489	101393	24	13	4	7	676	15	590	71	39	0	0	39	0	1004	0	0	1004	0	64	55	0	1214	33253	0	2	5	10	23391	626	1	2	13	14	4	584	6	0	41310	27	4	555	471	11	40189	43	0
4	Animales	1987	84350	43	13	2	28	861	12	515	334	175	10	1	153	11	7269	196	1	6602	470	43	20	68	1015	756	6200	6	32	14	722	1132	0	3	6	25	32	1050	0	16	58676	0	21	124	339	486	57502	0	204
5	Aves	666	51190	15	7	1	7	566	1	503	62	117	2	0	110	5	6200	21	0	5985	194	16	4	68	640	672	6200	0	0	3	671	629	0	0	3	7	16	603	0	0	48534	0	0	33	123	381	47997	0	0
6	Bromelias, labiadas y pasifloras	52	4022	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3	1	0	63	1	0	0	0	0	0	15	0	0	0	0	0	15	0	0	316	0	0	0	0	0	316	0	0
7	Cactus	6	109	0	0	0	0	0	0	0	0	3	0	0	3	0	68	0	0	68	0	0	0	0	0	0	0	0	0	0	0	5	0	0	0	0	0	5	0	0	107	0	0	0	0	0	107	0	0
8	Corales	21	72	1	0	0	1	4	0	0	4	19	0	0	19	0	66	0	0	66	0	0	0	0	0	0	0	0	0	0	0	21	0	0	1	2	0	18	0	0	71	0	0	2	4	0	65	0	0
9	Crustáceos	14	567	2	0	0	2	5	0	0	5	0	0	0	0	0	0	0	0	0	0	0	1	0	0	2	0	1	1	1	2	2	0	0	0	0	0	1	0	1	5	0	0	0	0	0	2	0	3
10	Decápodos	11	120	2	0	0	2	5	0	0	5	0	0	0	0	0	0	0	0	0	0	0	1	0	0	2	0	1	1	1	2	2	0	0	0	0	0	1	0	1	5	0	0	0	0	0	2	0	3
11	Dipteros	55	2189	0	nan	nan	nan	0	nan	nan	nan	0	nan	nan	nan	nan	0	nan	nan	nan	nan	0	0	nan	0	0	nan	nan	nan	nan	nan	0	nan	nan	nan	nan	nan	nan	nan	nan	0	nan	nan	nan	nan	nan	nan	nan	nan
12	Escarabajos	71	4477	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1	0	0	1	0	0	0	1	1	3	0	0	0	0	0	3	0	0	57	0	0	0	0	0	57	0	0
13	Especies Maderables	10	192	9	5	3	1	191	11	150	30	1	0	0	1	0	1	0	0	1	0	0	0	0	0	0	0	0	0	0	0	7	0	0	3	2	0	1	0	0	185	0	0	55	99	0	30	0	0
14	Fanerógamas	20	1319	5	2	1	2	15	1	10	4	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	10	0	0	0	1	0	8	0	0	1125	0	0	0	3	0	1121	0	0
15	Frailejones	1	4	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1	0	0	0	0	0	1	0	0	4	0	0	0	0	0	4	0	0
16	Gimnospermas	7	19014	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3	0	0	18684	0	0	0	2	9106	2	0	0	0	1	0	1	0	0	4	0	0	0	3	0	1	0	0
17	Helechos	30	591	0	0	0	0	0	0	0	0	5	0	0	5	0	52	0	0	52	0	0	2	0	0	45	0	0	0	1	19	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0
18	Hepáticas	32	3015	0	nan	nan	nan	0	nan	nan	nan	0	nan	nan	nan	nan	0	nan	nan	nan	nan	0	0	nan	0	0	nan	nan	nan	nan	nan	0	nan	nan	nan	nan	nan	nan	nan	nan	0	nan	nan	nan	nan	nan	nan	nan	nan
19	Hongos	199	22858	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1	0	0	89	0	0	0	0	0	0	1	0	0	0	0	0	1	0	0	9	0	0	0	0	0	9	0	0
20	Hormigas	81	1860	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3	0	0	21	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0
21	Insectos	669	18114	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	5	0	0	53	0	0	0	1	1	17	0	0	0	0	0	14	0	3	133	0	0	0	0	0	115	0	18
22	Invertebrados	779	20134	4	0	0	4	11	0	0	11	21	0	0	21	0	71	0	0	71	0	0	6	0	0	55	0	1	1	2	3	45	0	0	1	2	0	38	0	4	223	0	0	2	4	0	196	0	21
23	Líquenes	178	20688	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1	0	0	89	0	0	0	0	0	0	1	0	0	0	0	0	1	0	0	9	0	0	0	0	0	9	0	0
24	Magnolias y afines	8	324	1	0	0	1	31	0	0	31	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	3	0	0	0	1	0	2	0	0	114	0	0	0	3	0	111	0	0
25	Mamíferos	155	4616	5	1	1	3	151	11	6	134	25	8	0	12	5	699	175	0	261	263	7	3	0	195	3	0	0	0	2	2	133	0	1	1	7	4	114	0	6	3466	0	11	6	158	22	3174	0	95
26	Mariposas	399	6143	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	9	0	0	0	0	0	9	0	0	54	0	0	0	0	0	54	0	0
27	Medusas	5	47	0	0	0	0	0	0	0	0	2	0	0	2	0	5	0	0	5	0	0	0	0	0	0	0	0	0	0	0	2	0	0	0	0	0	2	0	0	5	0	0	0	0	0	5	0	0
28	Microorganismos	272	6829	0	nan	nan	nan	0	nan	nan	nan	0	nan	nan	nan	nan	0	nan	nan	nan	nan	0	0	nan	0	0	nan	nan	nan	nan	nan	0	nan	nan	nan	nan	nan	nan	nan	nan	0	nan	nan	nan	nan	nan	nan	nan	nan
29	Musgos	94	3779	0	nan	nan	nan	0	nan	nan	nan	0	nan	nan	nan	nan	0	nan	nan	nan	nan	0	0	nan	0	0	nan	nan	nan	nan	nan	0	nan	nan	nan	nan	nan	nan	nan	nan	0	nan	nan	nan	nan	nan	nan	nan	nan
30	Orquídeas	52	1853	1	0	0	1	4	0	0	4	30	0	0	30	0	753	0	0	753	0	2	2	0	54	147	0	0	0	0	0	1	0	0	0	0	0	1	0	0	59	0	0	0	0	0	59	0	0
31	Palmas	29	1493	5	3	1	1	85	4	80	1	0	0	0	0	0	0	0	0	0	0	2	2	0	2	113	0	0	0	1	15	4	0	0	2	1	0	1	0	0	14	0	0	3	9	0	2	0	0
32	Peces	207	2619	12	3	0	9	74	0	4	70	0	0	0	0	0	0	0	0	0	0	20	5	0	180	20	0	4	28	6	43	172	0	1	0	3	6	156	0	6	1909	0	9	0	23	26	1763	0	88
33	Peces Marinos	86	864	2	1	0	1	4	0	2	2	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	81	0	0	0	1	2	75	0	3	769	0	0	0	1	10	749	0	9
34	Peces dulceacuícolas	45	773	3	0	0	3	58	0	0	58	0	0	0	0	0	0	0	0	0	0	20	0	0	180	0	0	0	0	1	23	31	0	1	0	0	1	27	0	2	548	0	9	0	0	8	459	0	72
35	Plantas	1663	128854	24	13	4	7	676	15	590	71	44	0	0	44	0	1056	0	0	1056	0	64	60	0	1214	51982	0	2	5	13	32516	628	1	2	13	15	4	585	6	0	41314	27	4	555	474	11	40190	43	0
36	Reptiles	108	2333	5	2	0	3	37	0	2	35	11	0	1	9	1	150	0	1	136	13	0	1	0	0	3	0	0	0	0	0	87	0	1	0	1	1	84	0	0	1630	0	1	0	1	7	1621	0	0
37	Vertebrados	1208	64156	39	13	2	24	850	12	515	323	154	10	1	132	11	7198	196	1	6531	470	43	14	68	1015	701	6200	5	31	12	719	1087	0	3	5	23	32	1012	0	12	58453	0	21	122	335	486	57306	0	183
38	Zamias	nan	1	0	nan	nan	nan	0	nan	nan	nan	0	nan	nan	nan	nan	0	nan	nan	nan	nan	0	0	nan	0	0	nan	nan	nan	nan	nan	0	nan	nan	nan	nan	nan	nan	nan	nan	0	nan	nan	nan	nan	nan	nan	nan	nan
0	Equinodermos	30	608	0	nan	nan	nan	0	nan	nan	nan	0	nan	nan	nan	nan	0	nan	nan	nan	nan	0	0	nan	0	0	nan	nan	nan	0	0	2	nan	nan	nan	nan	nan	2	nan	nan	7	nan	nan	nan	nan	nan	7	nan	nan
1	Esponjas	16	59	0	nan	nan	nan	0	nan	nan	nan	0	nan	nan	nan	nan	0	nan	nan	nan	nan	0	0	nan	0	0	nan	nan	nan	nan	nan	0	nan	nan	nan	nan	nan	nan	nan	nan	0	nan	nan	nan	nan	nan	nan	nan	nan
2	Moluscos	5	171	0	nan	nan	nan	0	nan	nan	nan	0	nan	nan	nan	nan	0	nan	nan	nan	nan	0	0	nan	0	0	nan	nan	nan	0	0	1	nan	nan	nan	nan	nan	1	nan	nan	2	nan	nan	nan	nan	nan	2	nan	nan
3	Pinos y afines	6	19010	0	nan	nan	nan	0	nan	nan	nan	0	nan	nan	nan	nan	0	nan	nan	nan	nan	0	3	nan	0	18684	nan	nan	nan	2	9106	1	nan	nan	nan	nan	nan	1	nan	nan	1	nan	nan	nan	nan	nan	1	nan	nan
