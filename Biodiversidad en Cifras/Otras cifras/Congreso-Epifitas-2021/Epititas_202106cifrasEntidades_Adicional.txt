publishingOrganizationKey	organization	Nombre corto	typeOrg	registros	Especies	URLSocio
	Arizona State University Biocollections		Internacionales	2	2.0	
c803f6f5-2c6a-4b41-8c15-768d48ef1c8c	Asociación de Becarios del Casanare	ABC	ONG	143	16.0	https://sibcolombia.net/socios/asociacion-de-becarios-del-casanare-abc/
a2f1c6f5-88de-4fc5-891a-336259f32f4e	Asociación para el estudio y conservación de las aves acuáticas en Colombia - Calidris	CALIDRIS	ONG	17	16.0	https://sibcolombia.net/socios/asociacion-para-el-estudio-y-conservacion-de-las-aves-acuaticas-en-colombia-calidris/
	Australia's Virtual Herbarium		Internacionales	16		
	Berkeley Natural History Museums		Internacionales	7	5.0	
	Bernice Pauahi Bishop Museum		Internacionales	1	1.0	
8e6bc843-c1b4-4b10-b546-881f06049004	Biotica Consultores Ltda	Biotica	Empresas	3	3.0	https://sibcolombia.net/socios/biotica-consultores/
	Botanic Garden and Botanical Museum Berlin		Internacionales	128	86.0	
	Botanical Institute of Barcelona (IBB-CSIC-ICUB)		Internacionales	10	10.0	
7a079928-aee9-418a-b083-6152d01c78d6	CDMB - Corporación Autónoma Regional Para la Defensa de la Meseta de Bucaramanga	CDMB	Autoridades Ambientales	150	66.0	https://sibcolombia.net/socios/corporacion-autonoma-regional-para-la-defensa-de-la-meseta-de-bucaramanga-cdmb-jardin-botanico-eloy-valenzuela/
1106e179-e49f-461f-95a6-459bf4d53c1b	CORNARE - Corporación Autónoma Regional de las cuencas de los ríos Negro y Nare	Cornare	Autoridades Ambientales	80	55.0	https://sibcolombia.net/socios/cornare/
	CSIC-Real Jardín Botánico		Internacionales	619	225.0	
06867940-0867-4b4a-abb2-a57a16fcf2dc	CVC - Corporación Autónoma Regional del Valle del Cauca	CVC	Autoridades Ambientales	11	4.0	https://sibcolombia.net/socios/corporacion-autonoma-regional-del-valle-del-cauca-cvc/
	California Academy of Sciences		Internacionales	167	93.0	
	Canadian Museum of Nature		Internacionales	24	10.0	
1a4f4e64-eb3d-42c3-a359-1be3869b3a20	Cerro Matoso S.A	Cerromatoso	Empresas	304	15.0	https://sibcolombia.net/socios/cerro-matoso/
	ComisiÃ³n nacional para el conocimiento y uso de la biodiversidad		Internacionales	77	38.0	
	Comissão Executiva do Plano da Lavoura Cacaueira – Centro de Pesquisas do Cacau		Internacionales	1	1.0	
	Conservatoire et Jardin botaniques de la Ville de Genève - G		Internacionales	140	77.0	
4b3fc3ac-227f-477d-9853-cfa76044d108	Cormacarena - Corporación para el Desarrollo Sostenible del área de Manejo Especial La Macarena	Cormacarena	Autoridades Ambientales	2		https://sibcolombia.net/socios/corporacion-para-el-desarrollo-sostenible-del-area-de-manejo-especial-la-macarena-cormacarena/
015d5ac7-2644-49e9-815e-79468647d6af	Corpocaldas - Corporación Autónoma Regional de Caldas	Corpocaldas	Autoridades Ambientales	2	1.0	https://sibcolombia.net/socios/corporacion-autonoma-regional-de-caldas/
69cbe3e1-ea7a-4f22-b4b8-47dd4df6e79b	Corpoguavio - Corporación Autónoma Regional del Guavio	Corpoguavio	Autoridades Ambientales	188	3.0	https://sibcolombia.net/socios/corporacion-autonoma-regional-del-guavio-corpoguavio/
47844d46-753c-44c5-8a1d-b50fa69f7ddc	Corporación Cuenca Verde	CCV	ONG	10	10.0	https://sibcolombia.net/socios/corporacion-cuenca-verde/
2627e955-93f5-4206-bac5-a1e3bd91ee37	Corporación Paisajes Rurales	Paisajes Rurales	ONG	4	4.0	https://sibcolombia.net/socios/corporacion-paisajes-rurales/
1904954c-81e7-4254-9778-ae3deed93de6	Corporación San Jorge	Corp. San Jorge	ONG	38	37.0	https://sibcolombia.net/socios/corporacion-san-jorge/
	Dep. of Plant Biology and Ecology, Univ. Seville		Internacionales	3	2.0	
	Duke University Herbarium		Internacionales	9	5.0	
	European Nucleotide Archive (EMBL-EBI)		Internacionales	8	6.0	
	Fairchild Tropical Botanic Garden		Internacionales	1	1.0	
fe602f47-b553-4291-b6e5-197b9837e167	Federación Nacional de Cafeteros de Colombia	Cafeteros de Colombia	Gremios Empresariales	4	3.0	https://sibcolombia.net/socios/federacion-nacional-de-cafeteros-de-colombia/
	Field Museum		Internacionales	1245	398.0	
	Florida Museum of Natural History		Internacionales	12	11.0	
05827c69-a802-472f-bbe3-76629dfd57a7	Fundación Alma	Fund. Alma	ONG	2	2.0	https://sibcolombia.net/socios/fundacion-alma/
4dad347c-f297-46ee-9755-fda443b966d7	Fundación Ecohabitats	Fund. Ecohabitats 	ONG	25	25.0	https://sibcolombia.net/socios/fundacion-ecohabitats/
00a915e7-b4e2-4795-bcbf-45e4dda0e927	Fundación Estación Biológica Guayacanal	Fund. Guayacanal	ONG	6	5.0	https://sibcolombia.net/socios/fundacion-estacion-biologica-guayacanal/
cd9bc4b5-4375-4991-aec5-0b4443b5d7a6	Fundación Gaia	Fund. GAIA	ONG	21	14.0	https://sibcolombia.net/socios/fundacion-gaia/
85aae44a-2a4c-4a3f-92cc-a1a8d27b90fa	Fundación Humedales	Fund. Humedales	ONG	1	1.0	https://sibcolombia.net/socios/fundacion-humedales/
f52593de-ac30-49ea-8e3e-07cf745249ec	Fundación Natura Colombia	Fund. Natura	ONG	49	6.0	https://sibcolombia.net/socios/fundacion-natura/
fb92ab7b-65fe-4353-9c4b-99ee81c91feb	Fundación Reserva Natural La Palmita - Centro de Investigación	Fund. La Palmita	ONG	5	4.0	 https://sibcolombia.net/socios/fundacion-reserva-natural-la-palmita-centro-de-investigacion/
8825eec2-5312-4e2a-ada4-41b907818fdf	Fundación Trópico	Fund. Trópico	ONG	3	2.0	https://sibcolombia.net/socios/fundacion-tropico/
a0f40644-1fec-42d8-af64-0bce3e9d76d2	Fundación Trópico Alto	Fund. Trópico Alto	ONG	3	3.0	https://sibcolombia.net/socios/fundacion-tropico-alto/
	GBIF-Sweden		Internacionales	138	84.0	
	Georg-August-Universität Göttingen, Albrecht-von-Haller-Institut für Pflanzenwissenschaften, Abteilung Systematische Botanik		Internacionales	394	62.0	
2977895d-3ce2-4fb9-b62e-a775c8fd9304	Grupo Energía Bogotá	GEB	Empresas	978	18.0	https://sibcolombia.net/socios/grupo-energia-bogota/
	Harvard University Herbaria		Internacionales	799	335.0	
	Herbarium Hamburgense		Internacionales	13	13.0	
	Herbarium of Université de Montpellier 2, Institut de Botanique		Internacionales	4	4.0	
	Herbarium of the University of Aarhus		Internacionales	4	4.0	
9d77fdeb-100f-4b29-98ad-4effdd824457	Instituto Amazónico de Investigaciones Científicas - SINCHI	Sinchi	Centros/Institutos de Investigación	2432	198.0	https://sibcolombia.net/socios/instituto-amazonico-de-investigaciones-cientificas-sinchi/
	Instituto Anchietano de Pesquisas/UNISINOS		Internacionales	1	1.0	
	Instituto Nacional de Pesquisas da Amazônia - INPA		Internacionales	25	24.0	
	Instituto de Botánica Darwinion - CONICET		Internacionales	1	1.0	
	Instituto de Botânica, São Paulo		Internacionales	37	19.0	
e1050db2-9faf-4d72-b860-295debaf9d2a	Instituto de Investigaciones Ambientales del Pacífico John Von Neumann - IIAP	IIAP	Centros/Institutos de Investigación	173	77.0	https://sibcolombia.net/socios/instituto-de-investigaciones-ambientales-del-pacifico-john-von-neumann-iiap/
2a7e3080-28a9-11dd-97cd-b8a03c50a862	Instituto de Investigación de Recursos Biológicos Alexander von Humboldt	IAvH	Centros/Institutos de Investigación	5190	414.0	https://sibcolombia.net/socios/instituto-de-investigacion-de-recursos-biologicos-alexander-von-humboldt/
	Instituto de Pesquisas Jardim Botanico do Rio de Janeiro		Internacionales	30	22.0	
a7e6d0ba-9e3d-4be2-b3ac-2c5e812e0a31	Instituto para la Investigación y la Preservación del Patrimonio Cultural y Natural del Valle del Cauca - INCIVA	INCIVA	Centros/Institutos de Investigación	19	11.0	https://sibcolombia.net/socios/instituto-para-la-investigacion-y-la-preservacion-del-patrimonio-cultural-y-natural-del-valle-del-cauca-inciva/
	Jardin Botanique de Montréal		Internacionales	2	2.0	
eace4687-50e8-4f9a-829b-29ff8ff1fa8b	"Jardín Botánico de Bogotá ""José Celestino Mutis"""	JBB	Entidades Administrativas Territoriales	1700	294.0	https://sibcolombia.net/socios/jardin-botanico-de-bogota-jose-celestino-mutis/
698acf43-05cd-4b45-8107-7c666d87f77c	"Jardín Botánico de Cartagena ""Guillermo Piñeres"""	JBC	ONG	36	11.0	https://sibcolombia.net/socios/herbario-jardin-botanico-de-cartagena-guillermo-pineres/
	Lund Botanical Museum (LD)		Internacionales	20	19.0	
	MNHN - Museum national d'Histoire naturelle		Internacionales	464	195.0	
	Meise Botanic Garden		Internacionales	59	37.0	
	Michigan State University Herbarium		Internacionales	1	1.0	
	Missouri Botanical Garden		Internacionales	12276	1637.0	
	Museo Argentino de Ciencias Naturales		Internacionales	1	1.0	
	Museo Nacional de Costa Rica		Internacionales	16	15.0	
	Museu Botânico Municipal		Internacionales	88	56.0	
	Museu Paraense EmÃ­lio Goeldi		Internacionales	7	6.0	
	National Museum of Natural History, Smithsonian Institution		Internacionales	3385	505.0	
	National Museum of Nature and Science, Japan		Internacionales	7	6.0	
	Natural History Museum		Internacionales	230	120.0	
	Natural History Museum, Vienna - Herbarium W		Internacionales	42	29.0	
	Naturalis Biodiversity Center		Internacionales	732	258.0	
c3da1f49-b2c8-4751-b72f-28855546ec4c	Oleoducto Bicentenario	Oleoducto Bicentenario	Empresas	496	13.0	https://sibcolombia.net/socios/oleoducto-bicentenario/
ab13adb9-ce23-444d-87c9-ce41f03ef2b3	Parques Nacionales Naturales de Colombia	PNN	Autoridades Ambientales	853	15.0	https://sibcolombia.net/socios/parques-nacionales-naturales-de-colombia/
190b47cb-54d5-4b87-9c1e-22b0483fe071	"Patrimonio Natural Fondo para la Biodiversidad y Áreas Protegidas ""Patrimonio Natural"""	Patrimonio Natural	ONG	1	1.0	https://sibcolombia.net/socios/patrimonio-natural-fondo-para-la-biodiversidad-y-areas-protegidas-patrimonio-natural/
	PlutoF		Internacionales	16	6.0	
0e2f2e28-7790-4c82-b8fb-6ef7b4c764e2	Pontificia Universidad Javeriana	PUJ	Academia	1366	387.0	https://sibcolombia.net/socios/pontificia-universidad-javeriana/
dbc2ab56-d499-403c-8db5-c1a49cd0b75f	Promigas S.A E.S.P	Promigas	Empresas	298	4.0	https://sibcolombia.net/socios/promigas/
278c2395-6edb-41f4-8f0a-0abd13656901	Red Nacional de Jardines Botánicos de Colombia	RNJB	Redes/Iniciativas	2	2.0	https://sibcolombia.net/socios/red-nacional-de-jardines-botanicos-de-colombia/
	Royal Botanic Garden Edinburgh		Internacionales	60	47.0	
	Royal Botanic Gardens, Kew		Internacionales	361	204.0	
e70c4151-0d1a-414d-b70e-e87ac1e812b7	Secretaría Distrital de Ambiente	SDA	Autoridades Ambientales	3	1.0	https://sibcolombia.net/socios/secretaria-distrital-de-ambiente/
	Société des Sciences Naturelles et Mathématiques de Cherbourg		Internacionales	1	1.0	
	Staatliche Naturwissenschaftliche Sammlungen Bayerns		Internacionales	14	11.0	
f5db868f-e5bf-4208-bd9d-d4063ae1c825	TERRASOS	Terrasos	Empresas	23	7.0	https://sibcolombia.net/socios/terrasos/
	The New York Botanical Garden		Internacionales	2170	399.0	
	UNIBIO, IBUNAM		Internacionales	60	27.0	
	USF Water Institute		Internacionales	97	47.0	
	UiO Natural History Museum		Internacionales	1	1.0	
	United Herbaria of the University and ETH Zurich (Z+ZT)		Internacionales	40	4.0	
c8f840a3-4949-4e18-82e9-5771c3e57129	Universidad Católica de Oriente	UCO	Academia	83	43.0	https://sibcolombia.net/socios/universidad-catolica-de-oriente/
b8cd2cdb-ee95-409c-b1b8-e09bab4f9a70	Universidad Distrital Francisco José de Caldas	UDistrital	Academia	185	85.0	https://sibcolombia.net/socios/universidad-distrital-francisco-jose-de-caldas/
7d91f9bd-f6cd-48e3-ba81-3c228cf5e13a	Universidad Icesi	ICESI	Academia	63	28.0	https://sibcolombia.net/socios/universidad-icesi/
6c147991-c3bf-453d-a778-3bea9a534804	Universidad Industrial de Santander	UIS	Academia	344	105.0	https://sibcolombia.net/socios/universidad-industrial-de-santander-uis/
eac88d99-9f6c-4031-8fc4-8088f0e0dfe7	Universidad Nacional de Colombia	UNAL	Academia	17752	1098.0	https://sibcolombia.net/socios/universidad-nacional-de-colombia/
ad3f9c5f-5021-45a3-a7c4-3e64895f6f79	Universidad Pedagógica y Tecnológica de Colombia	UPTC	Academia	470	115.0	https://sibcolombia.net/socios/universidad-pedagogica-y-tecnologica-de-colombia/
06f46c98-9794-4d96-a014-aecdf24dbd7e	Universidad Tecnológica de Pereira	UTP	Academia	93	31.0	https://sibcolombia.net/socios/universidad-tecnologica-de-pereira/
073e52d4-44bd-41d7-bdfa-88c2735c694b	Universidad Tecnológica del Chocó	UTCH	Academia	178	67.0	https://sibcolombia.net/socios/universidad-tecnologica-del-choco/
cccff716-2694-4209-9f9e-2f7a484465a0	Universidad de Antioquia	UdeA	Academia	3023	341.0	https://sibcolombia.net/socios/universidad-de-antioquia/
f7f9717e-9e50-4a00-a30f-7b134390a566	Universidad de Caldas	UCaldas	Academia	542	160.0	https://sibcolombia.net/socios/universidad-de-caldas/
dec5e6c9-0156-4fa0-b01c-e642dbff48fc	Universidad de Córdoba	UniCórdoba	Academia	12	5.0	https://sibcolombia.net/socios/universidad-de-cordoba/ 
58c7e325-82fc-446d-9406-851b4d357db7	Universidad de Nariño	UdeNar	Academia	683	136.0	https://sibcolombia.net/socios/universidad-de-narino/
96b23685-f195-4131-af29-ea9e160225dd	Universidad de Pamplona	UPamplona	Academia	104	25.0	https://sibcolombia.net/socios/universidad-de-pamplona/
	Universidad de Salamanca. Nucleus		Internacionales	9	8.0	
256035fe-75ff-4a7c-94bc-86af590c9050	Universidad de la Amazonia	UniAmazonia	Academia	56	36.0	https://sibcolombia.net/socios/universidad-de-la-amazonia/
478a9e81-e716-42dc-a68d-03487953a32e	Universidad de la Salle	La Salle	Academia	177	65.0	https://sibcolombia.net/socios/universidad-de-la-salle/
77c64839-4c99-4a40-beb3-cd16afc23540	Universidad de los Andes	UniAndes	Academia	35	10.0	https://sibcolombia.net/socios/universidad-de-los-andes/
2fff5d0c-6bbd-432d-8832-cc4e307a267f	Universidad de los Llanos	UniLlanos	Academia	2	2.0	https://sibcolombia.net/socios/universidad-de-los-llanos/
bc709e2f-6eb4-4cbe-a295-e12eed0679f2	Universidad del Quindío	UniQuindío	Academia	134	30.0	https://sibcolombia.net/socios/universidad-del-quindio/
5a45153b-bdf9-44ae-b7a7-e3261896540b	Universidad del Tolima	UT	Academia	13	11.0	https://sibcolombia.net/socios/universidad-del-tolima/
85be57ed-f187-49c9-b7ff-eaa622e06217	Universidad del Valle	UniValle	Academia	62	27.0	https://sibcolombia.net/socios/universidad-del-valle/
	Universidade Estadual de Campinas - Instituto de Biologia		Internacionales	5	3.0	
	Universidade de Caxias do Sul		Internacionales	1	1.0	
	Universidade de S達o Paulo		Internacionales	16	10.0	
	University of Alabama Biodiversity and Systematics		Internacionales	2	2.0	
	University of Amsterdam / IBED		Internacionales	533	33.0	
	University of Connecticut		Internacionales	1	1.0	
	University of Michigan Herbarium		Internacionales	38	28.0	
	University of Tennessee Herbarium		Internacionales	9	5.0	
	University of Vienna - Herbarium WU		Internacionales	6	6.0	
	Université de Montréal Biodiversity Centre		Internacionales	1	1.0	
0c23482f-89f3-4efa-b6ed-7b25dadde4fc	WCS Colombia	WCS	ONG	2	2.0	https://sibcolombia.net/socios/wildlife-conservation-society-wcs-colombia/ 
	iNaturalist.org		Internacionales	727	193.0	
	naturgucker.de		Internacionales	4	3.0	
