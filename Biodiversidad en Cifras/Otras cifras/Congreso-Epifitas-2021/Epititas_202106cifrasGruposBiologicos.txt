	grupoBio	grupoBio	registros	especies	registrosCites	registrosEndemicas	registrosAmenaza	registrosthreatStatus_UICN	especiesCites	especiesEndemicas	especiesAmenaza	especiesthreatStatus_UICN	registrosCitesII	especiesCitesII	registrosCR	registrosEN	registrosVU	especiesCR	especiesEN	especiesVU	registrosCR_UICN	registrosDD_UICN	registrosEN_UICN	registrosLC_UICN	registrosNT_UICN	registrosVU_UICN	especiesCR_UICN	especiesDD_UICN	especiesEN_UICN	especiesLC_UICN	especiesNT_UICN	especiesVU_UICN
0	Angiospermas	Angiospermas	52556	1853	6708	8675	807	9601	693	810	106	91	6708	693	226	326	255	13	42	51	17	37	73	9111	199	164	5	6	9	60	5	6
1	Antocherotales	Antocherotales	29	1	0	0	0	0	0	0	0	0	nan	nan	nan	nan	nan	nan	nan	nan	nan	nan	nan	nan	nan	nan	nan	nan	nan	nan	nan	nan
2	Araceae	Araceae	15779	338	0	2657	0	846	0	116	0	10	0	0	0	0	0	0	0	0	0	0	10	814	0	22	0	0	1	8	0	1
3	Bromeliaceae	Bromeliaceae	5687	139	0	332	179	802	0	37	32	21	0	0	37	54	88	10	10	12	10	0	33	746	0	13	1	0	5	13	0	2
4	Cactaceae	Cactaceae	612	7	601	0	0	565	5	0	0	5	601	5	0	0	0	0	0	0	0	0	0	564	1	0	0	0	0	4	1	0
5	Cyclanthaceae	Cyclanthaceae	1213	53	0	175	0	25	0	16	0	2	0	0	0	0	0	0	0	0	0	0	0	25	0	0	0	0	0	2	0	0
6	Ericaceae	Ericaceae	9863	83	0	1903	0	6375	0	51	0	18	0	0	0	0	0	0	0	0	4	29	26	6054	165	97	1	1	1	10	3	2
7	Gesneriaceae	Gesneriaceae	6253	85	0	393	0	0	0	13	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0
8	Hepáticas	Hepáticas	8065	183	0	44	0	137	0	4	0	1	0	0	0	0	0	0	0	0	0	0	0	137	0	0	0	0	0	1	0	0
9	Melastomataceae	Melastomataceae	2113	37	0	634	0	648	0	18	0	11	0	0	0	0	0	0	0	0	0	0	0	583	33	32	0	0	0	9	1	1
10	Musgos	Musgos	3422	24	0	1	1	0	0	1	1	0	0	0	1	0	0	1	0	0	0	0	0	0	0	0	0	0	0	0	0	0
11	Orchidaceae	Orchidaceae	10816	1095	6088	2569	628	332	683	556	74	22	6088	683	189	272	167	3	32	39	3	8	4	317	0	0	3	5	2	12	0	0
12	Plantas	Plantas	64072	2061	6708	8720	808	9738	693	815	107	92	6708	693	227	326	255	14	42	51	17	37	73	9248	199	164	5	6	9	61	5	6
